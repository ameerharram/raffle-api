<?php
/*
|--------------------------------------------------------------------------
| Auth Routes
| Routes shared between User (frontend) and  Admin (Dashboard) Parts with no authenticaiton
|--------------------------------------------------------------------------
*/


Route::group(['prefix' => 'admin'], function () {
	/*Open Routes*/
	Route::post('login','AuthController@loginAdmin');

	/*Restericted Routes*/
	Route::group(['middleware' => [ 'jwt-auth','role.admin' ] ], function() {
		
		/*cleared routes*/
		Route::get('check-login','AuthController@checkLogin');
		Route::get('logout','AuthController@deAuthenticate');
		Route::get('token','AuthController@getApiToken');
		Route::get('token/refresh','AuthController@refreshToken');

		/*Dashboard Stats*/
		Route::get('dashboard-stats','DashboardController@dashboardStats');

		/*Users Resource Route...*/	
		Route::resource('users', 'UsersController');
		
		
		Route::resource('lotteries', 'LotteriesController');
		Route::get('lotteries/{lottery}/{drawCount}/draw-winner', 'LotteriesController@drawWinner');
		Route::post('lotteries/save-draw-result', 'LotteriesController@saveDrawResults');
		
		Route::resource('tickets', 'TicketsController');
		Route::resource('participants', 'ParticipantsController');
		
		Route::get('profile','UserController@userProfile');
		Route::post('profile-update','UserController@updateUserInfo');
		Route::post('profile-change-password','AuthController@changePassword');
		Route::post('profile-change-password','AuthController@changePassword');
		
		/*Settings Routes*/
		Route::post('import-orders','SettingController@importOrders');

	});	

});



?>