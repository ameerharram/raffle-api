<?php
/*
|--------------------------------------------------------------------------
| Public api routes
| Routes shared with WP webiste 
|--------------------------------------------------------------------------
*/

Route::middleware(['jwt-auth','role.api'])->namespace('Api')->prefix('public')->group(function () {
    
    /*Open Routes*/
    Route::post('create-tickets','PublicApiController@createTicketFromCheckout');
    Route::get('get-winner/{lottery}','PublicApiController@getWinnerFromContest');
    Route::post('get-lottery-detail','PublicApiController@getParticipantLotteryDetails');

});

?>