<?php

use Illuminate\Database\Seeder;

class LotteriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Lotteries::class, 100)->create();
    }
}
