<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        //'gender' => $faker->randomElement(array('male','female')),
        'password' =>  Hash::make('secret'),
		// 'dob' => $faker->date(),
        'user_type' =>  $faker->randomElement(array('admin'))
		// 'user_type' =>  $faker->randomElement(array('admin', 'subadmin' , 'wpuser'))
		
    ];
});

$factory->define(App\Lotteries::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->company,
        'enteries' => 100,
        'start' => $faker->date(),
        'end' => $faker->date(),
        'announcment' => $faker->date(),
        'creator_id' => $faker->randomElement(array(1, 2 , 3))

    ];
});
