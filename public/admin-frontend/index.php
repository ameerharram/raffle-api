<!--
* INSPINIA - Responsive Admin Theme
* Version 2.7
*
-->

<!DOCTYPE html>
<html lang="en" ng-app="main">

<head>
    
    <meta charset="utf-8">
    <base href="/admin-frontend/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="shortcut icon" href="images/fevicon.png">
    <!-- Page title set in pageTitle directive -->
    <title page-title></title>



   <!-- bower not auto css -->
   <link href="bower_components/fontawesome/css/font-awesome.css" rel="stylesheet">
   <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  
   <!-- <link rel="stylesheet" href="bower_components/ladda/css/ladda-themeless.scss"> -->
   <!-- bower not auto css -->

    <!-- bower:css -->
    <link rel="stylesheet" href="bower_components/angularjs-toaster/toaster.css" />
    <link rel="stylesheet" href="bower_components/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="bower_components/ladda/dist/ladda.min.css" />
    <link rel="stylesheet" href="bower_components/AngularJS-Toaster/toaster.css" />
    <link rel="stylesheet" href="bower_components/angular-datatables/dist/css/angular-datatables.css" />
    <link rel="stylesheet" href="bower_components/angular-datatables/dist/plugins/bootstrap/datatables.bootstrap.min.css" />
    <link rel="stylesheet" href="bower_components/angular-fancy-modal/dist/angular-fancy-modal.css" />
    <!-- endbower -->
    
   <!-- Custom CSS -->
	 <link href="dist/css/main.min.css" rel="stylesheet">
	 <!-- Custom CSS -->
</head>

<body ng-controller="MainCtrl as main">
    <!-- Toaster Notifications -->
    <toaster-container></toaster-container>
    <!-- Main view  -->
    <div ui-view></div>   

<!-- bower not loaded auto js -->
   <!-- <script src="bower_components/ladda/dist/spin.min.js"></script> -->
   <!-- <script src="bower_components/ladda/js/ladda.js"></script> -->
<!-- bower not loaded auto js -->

<!-- bower:js -->
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/angular/angular.js"></script>
<script src="bower_components/angular-ui-router/release/angular-ui-router.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="bower_components/angular-animate/angular-animate.js"></script>
<script src="bower_components/angularjs-toaster/toaster.js"></script>
<script src="bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.js"></script>
<script src="bower_components/pace/pace.js"></script>
<script src="bower_components/spin.js/spin.js"></script>
<script src="bower_components/ladda/dist/ladda.min.js"></script>
<script src="bower_components/ladda-angular/dist/ladda-angular.js"></script>
<script src="bower_components/AngularJS-Toaster/toaster.js"></script>
<script src="bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="bower_components/angular-datatables/dist/angular-datatables.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/colreorder/angular-datatables.colreorder.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/columnfilter/angular-datatables.columnfilter.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/light-columnfilter/angular-datatables.light-columnfilter.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/colvis/angular-datatables.colvis.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/fixedcolumns/angular-datatables.fixedcolumns.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/fixedheader/angular-datatables.fixedheader.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/scroller/angular-datatables.scroller.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/tabletools/angular-datatables.tabletools.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/buttons/angular-datatables.buttons.js"></script>
<script src="bower_components/angular-datatables/dist/plugins/select/angular-datatables.select.js"></script>
<script src="bower_components/clipboard/dist/clipboard.js"></script>
<script src="bower_components/ngclipboard/dist/ngclipboard.js"></script>
<script src="bower_components/angular-sweetalert/SweetAlert.js"></script>
<script src="bower_components/angular-fancy-modal/dist/angular-fancy-modal.js"></script>
<!-- endbower -->

<!-- Custom JS -->
<script src="dist/js/main.js"></script>
<!-- Custom JS -->
</body>

</html>
