/**
 * pageTitle - Directive for set Page title - mata title
 */
(function () {
    'use strict';
    angular
        .module('main')
        .directive('pageTitle', pageTitle);

    function pageTitle($rootScope, $timeout) {
        return {
            link: function(scope, element) {
                var listener = function(event, toState, toParams, fromState, fromParams) {
                    // Default title - load on Dashboard 1
                    var title = 'Charity Moose | Raffle';
                    // Create your own title pattern
                    if (toState.data && toState.data.pageTitle) title = 'Charity Moose | ' + toState.data.pageTitle;
                    $timeout(function() {
                        element.text(title);
                    });
                };
                $rootScope.$on('$stateChangeStart', listener);
            }
        }
    }

})();