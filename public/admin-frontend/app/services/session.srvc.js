(function () {
    'use strict';

    angular
       .module('main')
       .service('SessionService', SessionService);

    function SessionService($rootScope){

        this.setToken = function(token){
            this.setCookie('_token', token);
        }

        this.getToken = function(){
            return this.getCookie('_token');
        }

        this.getLoginStatus = function(){
            return this.getCookie('_token') ? true : null;
        };

        this.setRememberMeFlag = function(){
            this.setCookie('remember_me_flag', true);
        };

        this.getRememberMeFlag = function(){
            return this.getCookie('remember_me_flag');
        };

        this.removeRememberMeFlag = function(){
            this.delete_cookie('remember_me_flag');
        };

        this.clearUserSession = function(){
            this.delete_cookie('_token');
            this.delete_cookie('remember_me_flag');
            localStorage.clear();
            sessionStorage.clear();
        };

        this.loginPage = function(state){
           
            state.go('login');

        }

        this.dashboardPage = function(state){
            state.go('index.main');
        }

        this.getCookie = function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return null;
        };

        this.setCookie = function(cname, cvalue) {
            var d = new Date();
            d.setTime(d.getTime() + (60*24*60*60*1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
        };

        this.setSessionCookie = function(cname, cvalue) {
            var expires = "expires=0";
            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
        };

        this.delete_cookie = function(name) {
            document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        };
    }
})();