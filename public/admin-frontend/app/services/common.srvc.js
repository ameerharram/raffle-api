(function () {
    'use strict';

    angular
       .module('main')
       .service('Common', Common);


    function Common($state, SessionService, UsersService){
        this.getUser = function(user)
        {


            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                   user(res.data.responseData);
                   
                    
                }else
                {
                    user(null);
                }
            }

            function failed(res)
            {
                user(null);
            }

            UsersService.
                        getLoggedInUser()
                        .then(success,failed);
        };

        this.logout = function()
        {
             function success(res){
                
                   SessionService.clearUserSession();
                
            }

            function failed(res)
            {
                SessionService.clearUserSession();
                SessionService.loginPage($state);
            }

            UsersService.
                        logout()
                        .then(success,failed).finally(function(){
                            SessionService.clearUserSession();
                            SessionService.loginPage($state);
                        });
            
        }
    }



})();



