(function () {
    'use strict';

    angular
        .module('main')
        .service('LoginService', function ($http,
                                                    $q,
                                                    $rootScope,
                                                    USER_LOGIN,
                                                    CHECK_LOGIN
                                                    ) {

            this.login = function (credentials) {
                return $http.post(USER_LOGIN , credentials);
            };

            this.checkLogin = function () {
                return $http.get(CHECK_LOGIN );
            };


        });  
})();  