(function () {
    'use strict';

    angular
        .module('main')
        .controller('LoginCtrl', LoginCtrl);

    function LoginCtrl($scope, $state, toaster ,LoginService ,SessionService ,CHECK_LOGIN) {
        
        /*Resolver Checks*/
        if(CHECK_LOGIN)
        {
            SessionService.dashboardPage($state);
            toaster.pop({
                        type: 'warning',
                        title: 'Fail',
                        body: 'Already Logged In.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }
        

        /*Properties*/
        var vm = this;
        vm.credentials =  null;
        vm.loading = false;
        
        /*Methods*/
        vm.login = login;
        





        /*Methods Implememntation*/
        function login(isValid)
        {
            if(!isValid) return false; // breaks function execution
            
            vm.loading = true;

            function success(res) {
                if ( res && res.data && res.data.status && res.data.type && res.data.status == 'success' && res.data.type == 'single-message' )
                {
                    SessionService.setToken(res.data.responseData.token);
                    SessionService.dashboardPage($state);                                       
                    console.log('its success');
                }
            }

            function failed(response) {
                // Notification.error(response.data.message);
            }

            LoginService
                        .login(vm.credentials)
                        .then(success,failed).finally(function() { vm.loading = false; });

        }
	}
})();