(function () {
    'use strict'; 

    angular
        .module('login', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('login', {
                url: "/login",
                templateUrl: "views/login.html",
                data: { pageTitle: 'Login view' },
                controller : 'LoginCtrl as vm',
                resolve: {
                    CHECK_LOGIN: ['$state','toaster','SessionService', function ($state,toaster,SessionService) {
                        return SessionService.getLoginStatus();
                        
                    }]
                }
            });
        
    }

})();
