(function () {
    'use strict';

    angular
        .module('main')
        .controller('ParticipantsCreateCtrl', ParticipantsCreateCtrl);

    function ParticipantsCreateCtrl($scope  , $filter, $stateParams , ParticipantsService){

        /*Properties*/
        var vm = this;
        vm.participant = null;
        vm.number_of_tickets = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.createParticipant = createParticipant;
        vm.createParticipantTickets = createParticipantTickets;
        vm.reset = reset;

        function createParticipant()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.participant = null;   
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

                       
            ParticipantsService.
                        createParticipant(vm.participant)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        
        }

        function createParticipantTickets()
        {
            vm.loading = true;
            //vm.number_of_tickets = $stateParams.participant_id; 
            var data = {'participant_id' : $stateParams.participant_id, 'number_of_tickets' : vm.number_of_tickets  };
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.number_of_tickets = null;   
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

                       
            ParticipantsService.
                        createParticipantTickets(data)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        
        }
        function reset() 
        {
            vm.participant = null;
            vm.errors = null;
            vm.loading = null;
            vm.number_of_tickets = null;   
        }    


        
    }
   
})();
 

