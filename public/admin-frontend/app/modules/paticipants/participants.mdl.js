(function () {
    'use strict'; 

    angular
        .module('participants', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('participants', {
                url: "/participants",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('participants.create', {
                url: "/create",
                templateUrl: "views/participants/create_participant.html",
                data: { pageTitle: 'Create Participant' },
                controller : 'ParticipantsCreateCtrl as vm'
                
            })
           .state('participants.create-ticket', {
                url: "/:participant_id/create-tickets",
                templateUrl: "views/participants/create_participant_tickets.html",
                data: { pageTitle: 'Create Ticket' },
                controller : 'ParticipantsCreateCtrl as vm'
                
            })
           .state('participants.list', {
                url: "/list",
                templateUrl: "views/participants/participants_list.html",
                data: { pageTitle: 'Participants List' },
                controller : 'ParticipantsListCtrl as vm'
            });
        
    }

})();
