(function () {
    'use strict';

    angular
        .module('main')
        .service('ParticipantsService', function ( $http,
                                                PARTICIPANTS,
                                                TICKETS         
                                           ) {

            this.getParticipants = function() {
                return $http.get(PARTICIPANTS);
            };

            this.createParticipant = function( participant ) {
                return $http.post( PARTICIPANTS , participant );
            };

            this.createParticipantTickets = function( tickets ) {
                return $http.post( TICKETS , tickets );
            };


        });  
})();  