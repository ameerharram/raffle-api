(function () {
    'use strict';

    angular
        .module('main')
        .controller('ParticipantsListCtrl', ParticipantsListCtrl);

    function ParticipantsListCtrl($scope ,toaster , DTOptionsBuilder , ParticipantsService){

        /*Properties*/
        var vm = this;
        vm.participants = null;
        vm.contentLoader = false;
        
        /*Methods*/
        vm.getParticipants = getParticipants;
        vm.commingSoong = commingSoong;

        init();


        function getParticipants()
        {
            vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.participants = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            ParticipantsService.
                        getParticipants()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }

        function init()
        {
            vm.getParticipants();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

    }
   
})();
 

