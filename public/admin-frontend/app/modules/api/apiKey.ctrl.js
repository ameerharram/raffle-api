(function () {
    'use strict';

    angular
        .module('main')
        .controller('ApiKeyCtrl', ApiKeyCtrl);

    function ApiKeyCtrl($rootScope, toaster , ApiService) {

    	/*Properties*/
        var vm = this;
        vm.token = null;
        vm.loading = null;
        
        /*Methods*/
        vm.getApiKey = getApiKey;
        vm.refreshToken = refreshToken;
        vm.copied = copied;

        init();

        function getApiKey()
        {

        	function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.token  = res.data.responseData.token;
                    
                }
            }

            function failed(res)
            {

            }

            ApiService.
                         getApiKey()
                        .then(success,failed).finally(function() {  });
        }

        function refreshToken()
        {
            vm.loading = true;

            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.token  = res.data.responseData.token;
                    
                }
            }

            function failed(res)
            {

            }

            ApiService.
                         getRefreshKey()
                        .then(success,failed).finally(function() { vm.loading = false });
        }

     	function init()
        {
            vm.getApiKey();
        }

        function copied() 
        {
            toaster.pop({
                        type: 'success',
                        title: 'API KEY',
                        body: 'Copied successfully.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

       
	}
})();
 