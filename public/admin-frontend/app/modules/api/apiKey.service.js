(function () {
    'use strict';

    angular
        .module('main')
        .service('ApiService', function (
                                              $http,
                                              GET_TOKEN,
                                              REFRESH_TOKEN
                                                   
                                             ) {

            this.getApiKey = function ( ) {
                return $http.get( GET_TOKEN );
            };

            this.getRefreshKey = function ( ) {
                return $http.get( REFRESH_TOKEN );
            };

        });  
})();  