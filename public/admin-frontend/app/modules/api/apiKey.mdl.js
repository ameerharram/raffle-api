(function () {
    'use strict'; 

    angular
        .module('api', ['ngclipboard'])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('api', {
                url: "/",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('api.token', {
                url: "api/refresh-token",
                templateUrl: "views/api/refresh_token.html",
                data: { pageTitle: 'Create Lottery' },
                controller : "ApiKeyCtrl as vm"
            });
        
    }

})();
