(function () {
    'use strict';

    angular
        .module('main')
        .controller('UsersCreateCtrl', UsersCreateCtrl);

    function UsersCreateCtrl($scope  , UsersService){

        /*Properties*/
        var vm = this;
        vm.user = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.createUser = createUser;
        vm.resetUser = resetUser;

        function createUser()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.user = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

            UsersService.
                        createUser(vm.user)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        }

        function resetUser() 
        {
            vm.user = null;
            vm.errors = null;
            vm.loading = false;
        }    


        
    }
   
})();
 

