(function () {
    'use strict';

    angular
        .module('main')
        .controller('UsersListCtrl', UsersListCtrl);

    function UsersListCtrl($scope ,toaster , DTOptionsBuilder , UsersService){

        /*Properties*/
        var vm = this;
        vm.users = null;
        
        /*Methods*/
        vm.getUsers = getUsers;
        vm.commingSoong = commingSoong;

        init();


        function getUsers()
        {
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.users = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            UsersService.
                        getUsers()
                        .then(success,failed).finally(function() {  });;
        }

        function init()
        {
            vm.getUsers();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

    }
   
})();
 

