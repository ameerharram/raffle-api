(function () {
    'use strict';

    angular
        .module('main')
        .service('UsersService', function ($http,
                                           USERS,
                                           USER_PROFILE,
                                           USER_LOGOUT        
                                           ) {

            this.getUsers = function() {
                return $http.get(USERS);
            };

            this.createUser = function(user) {
                return $http.post(USERS, user);
            };


            this.getLoggedInUser = function () {
                return $http.get(USER_PROFILE);

            }
            this.logout = function () {
                return $http.get(USER_LOGOUT );

            }


        });  
})();  