(function () {
    'use strict';

    angular
        .module('main')
        .controller('LandingCtrl', LandingCtrl);

    function LandingCtrl($rootScope, LandingService) {

    	/*Properties*/
        var vm = this;
        vm.stats =  null;
        /*Methods*/
        vm.getDashboardStats = getDashboardStats;

        init();

        function getDashboardStats()
        {

        	function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.stats  = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            LandingService.
                        getDashboardStats()
                        .then(success,failed).finally(function() {  });
        }

     	function init()
        {
            vm.getDashboardStats();
        }

       
	}
})();
 