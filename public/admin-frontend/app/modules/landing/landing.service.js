(function () {
    'use strict';

    angular
        .module('main')
        .service('LandingService', function (
                                              $http,
                                              DASHBOARD_STATS
                                                   
                                             ) {

            this.getDashboardStats = function ( ) {
                return $http.get( DASHBOARD_STATS );
            };

        });  
})();  