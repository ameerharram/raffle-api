(function () {
    'use strict';

    angular
        .module('main')
        .service('SettingsService', function ( $http,
                                                IMPORT_ORDERS         
                                           ) {

            this.importOrders = function( dates ) {
                return $http.post(IMPORT_ORDERS , dates );
            };

        });  
})();  