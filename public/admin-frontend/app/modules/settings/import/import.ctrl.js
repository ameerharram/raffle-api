(function () {
    'use strict';

    angular
        .module('main')
        .controller('ImportCtrl', ImportCtrl);

    function ImportCtrl($scope ,toaster , SettingsService ){

        /*Properties*/
        var vm = this;
        vm.dates = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.importOrders = importOrders;
        vm.reset = reset;

        function importOrders()
        {
            vm.loading = true;
            vm.errors = null;
            function success(res){

                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.dates = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }

            function failed(res)
            {

            }

            SettingsService.
                        importOrders(vm.dates)
                        .then(success,failed).finally(function() { vm.loading = false; });;

        }

        function reset() 
        {
            vm.dates = null;
            vm.errors = null;
            vm.loading = null;
        }  
        

    }
   
})();
 

