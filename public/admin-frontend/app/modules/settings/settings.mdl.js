(function () {
    'use strict'; 

    angular
        .module('settings', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('settings', {
                url: "/settings",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('settings.import', {
                url: "/import",
                templateUrl: "views/settings/import.html",
                data: { pageTitle: 'Import Data' },
                controller : "ImportCtrl as vm"
            });
        
    }

})();
