(function () {
    'use strict'; 

    angular
        .module('users', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('users', {
                url: "/users",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('users.create', {
                url: "/create",
                templateUrl: "views/users/create_user.html",
                data: { pageTitle: 'Create User' },
                controller : "UsersCreateCtrl as vm"
            })
           .state('users.list', {
                url: "/list",
                templateUrl: "views/users/user_list.html",
                data: { pageTitle: 'Users List' },
                controller : "UsersListCtrl as vm"
            });
        
    }

})();
