(function () {
    'use strict'; 

    angular
        .module('tickets', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('tickets', {
                url: "/tickets",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('tickets.create', {
                url: "/create",
                templateUrl: "views/tickets/create_ticket.html",
                data: { pageTitle: 'Create Lottery' }
            })
           .state('tickets.list', {
                url: "/list",
                templateUrl: "views/tickets/tickets_list.html",
                data: { pageTitle: 'tickets List' },
                controller : 'TicketsListCtrl as vm'
            });
        
    }

})();
