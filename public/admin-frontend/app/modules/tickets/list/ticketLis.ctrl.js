(function () {
    'use strict';

    angular
        .module('main')
        .controller('TicketsListCtrl', TicketsListCtrl);

    function TicketsListCtrl($scope ,toaster , DTOptionsBuilder , TicketsService){

        /*Properties*/
        var vm = this;
        vm.tickets = null;
        vm.contentLoader = false;
        
        /*Methods*/
        vm.getTickets = getTickets;
        vm.commingSoong = commingSoong;

        init();


        function getTickets()
        {
            vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.tickets = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            TicketsService.
                        getTickets()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }


        function getRefreshKey()
        {
           vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.tickets = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            TicketsService.
                        getTickets()
                        .then(success,failed).finally(function() { vm.contentLoader = false; }); 
        }

        function init()
        {
            vm.getTickets();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

      

    }
   
})();
 

