(function () {
    'use strict';

    angular
        .module('main')
        .service('TicketsService', function ( $http,
                                                TICKETS         
                                           ) {

            this.getTickets = function() {
                return $http.get(TICKETS);
            };


        });  
})();  