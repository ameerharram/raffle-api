(function () {
    'use strict'; 

    angular
        .module('lotteries', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('lotteries', {
                url: "/raffles",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('lotteries.create', {
                url: "/create",
                templateUrl: "views/lotteries/create_lottery.html",
                data: { pageTitle: 'Create Lottery' },
                controller : "LotteriesCreateCtrl as vm"
            })
           .state('lotteries.list', {
                url: "/list",
                templateUrl: "views/lotteries/lotteries_list.html",
                data: { pageTitle: 'Lotteries List' },
                controller : "LotteriesListCtrl as vm"
            })
           .state('lotteries.draw_winner', {
                url: "/:lottery_id/draw-winner",
                templateUrl: "views/lotteries/draw_winner.html",
                data: { pageTitle: 'Draw Winner' },
                controller : "DrawWinnerCtrl as vm"
            });
        
    }

})();
