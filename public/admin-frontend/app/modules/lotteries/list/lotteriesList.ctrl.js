(function () {
    'use strict';

    angular
        .module('main')
        .controller('LotteriesListCtrl', LotteriesListCtrl);

    function LotteriesListCtrl($scope ,toaster , $fancyModal , DTOptionsBuilder , LotteriesService ){

        /*Properties*/
        var vm = this;
        vm.lotteries = null;
        vm.contentLoader = false;
        
        /*Methods*/
        vm.getLotteries = getLotteries;
        vm.commingSoong = commingSoong;
        vm.demo = demo;

        init();


        function getLotteries()
        {
            vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.lotteries = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            LotteriesService.
                        getLotteries()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }

        function init()
        {
            vm.getLotteries();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

        function demo() {
            $fancyModal.open({ templateUrl: 'views/modals/winner.html' });
        };

        

    }
   
})();
 

