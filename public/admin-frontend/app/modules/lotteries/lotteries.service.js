(function () {
    'use strict';

    angular
        .module('main')
        .service('LotteriesService', function ( $http,
                                                LOTTERIES         
                                           ) {

            this.getLotteries = function() {
                return $http.get(LOTTERIES);
            };

            this.createLottery = function(lottery) {
                return $http.post(LOTTERIES, lottery);
            };

            this.getLotteryWinner = function(lottery_id ,drawCount) {
                return $http.get(LOTTERIES +'/'+ lottery_id + '/'+ drawCount +'/draw-winner');
            };

            this.saveDrawResult = function(winner) {
                return $http.post(LOTTERIES +'/save-draw-result' ,winner);
            };

        });  
})();  