(function () {
    'use strict';

    angular
        .module('main')
        .controller('LotteriesCreateCtrl', LotteriesCreateCtrl);

    function LotteriesCreateCtrl($scope  , $filter, LotteriesService){

        /*Properties*/
        var vm = this;
        vm.lottery = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.createLottery = createLottery;
        vm.reset = reset;

        function createLottery()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.lottery = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

            var data = angular.copy(vm.lottery);

            if(data && data.start)
            {
                data.start = $filter('date')(data.start ,'yyyy-MM-dd');
            }

            if(data && data.end)
            {
               data.end = $filter('date')(data.end ,'yyyy-MM-dd' ); 
            }

            if(data && data.announcment)
            {
                data.announcment = $filter('date')(data.announcment ,'yyyy-MM-dd' );
            }
            
            LotteriesService.
                        createLottery(data)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        
        }
        function reset() 
        {
            vm.lottery = null;
            vm.errors = null;
            vm.loading = null;
        }    


        
    }
   
})();
 

