(function () {
    'use strict';

    angular
        .module('main')
        .controller('DrawWinnerCtrl', DrawWinnerCtrl);

    function DrawWinnerCtrl($rootScope,$scope , $stateParams, $fancyModal, LotteriesService) {

    	/*Properties*/
        var vm = this;
        vm.lottery_id =  $stateParams.lottery_id;
        vm.participants =  null;
        vm.winner = null;
        vm.btnLoader =  false;
        vm.savebtnLoader =  false;
        vm.drawCount = 0;

        /*Methods*/
        vm.getLotteryWinner = getLotteryWinner;
        vm.saveDrawResult = saveDrawResult;
        vm.winnerPop = winnerPop;

        init();

        function getLotteryWinner()
        {
            vm.btnLoader = true;
            vm.participants =  null;
            vm.winner = null;
        	function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.participants = res.data.responseData.participants;
                    vm.winner = res.data.responseData.winner;
                    $scope.winner = vm.winner;
                    setTimeout(function(){
                        vm.winnerPop();
                    },1000);

                    
                }
            }

            function failed(res)
            {

            }
            vm.drawCount = vm.drawCount + 1;
            LotteriesService.
                        getLotteryWinner(vm.lottery_id , vm.drawCount )
                        .then(success,failed).finally(function() { vm.btnLoader = false;  });
        }

        function saveDrawResult() 
        {
            vm.savebtnLoader = true;
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                                     
                }
            }

            function failed(res)
            {

            }

            LotteriesService.
                        saveDrawResult(vm.winner)
                        .then(success,failed).finally(function() { vm.savebtnLoader = false;  });
        }

     	function init()
        {
            vm.getLotteryWinner();
        }

        function winnerPop() {
            $fancyModal.open({ templateUrl: 'views/modals/winner.html', scope:$scope });
        };


       
	}
})();
 