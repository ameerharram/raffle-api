(function () {
    'use strict';

    angular
        .module('main')
        .controller('NavBarCtrl', NavBarCtrl);

    function NavBarCtrl($scope , Common){

        /*Properties*/
        var vm = this;
        vm.user = null;
        vm.contentLoader = false;
        /*Methods*/
        $scope.logout = Common.logout;

        Common.getUser(function(user){
            if ( user )
            {
                $scope.user = user;
                $scope.user.userName = $scope.user.first_name +' '+ $scope.user.last_name;
            }
        });

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('TopNavBar', TopNavBar);

    function TopNavBar($scope , Common){

        /*Properties*/
        var vm = this;
        /*Methods*/
        $scope.logout = Common.logout;
    }
})();
 

