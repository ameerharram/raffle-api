(function () {
    'use strict';

    angular
        .module('main', [
            'ui.router',
            'ui.bootstrap',
            'oc.lazyLoad',
            'toaster',
            'ladda',
            'config',
            'datatables',
            'vesparny.fancyModal',
            //  'oitozero.ngSweetAlert',
            /*Custom Modules*/
            'login',
            'users',
            'lotteries',
            'tickets',
            'participants',
            'settings',
            'api'
        ])
        .config(config)
        .run();

    config.$inject = ['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider'];
    function config($stateProvider,$urlRouterProvider,$ocLazyLoadProvider) {
        $urlRouterProvider.otherwise('/dashboard');

        $ocLazyLoadProvider.config({
            //Set to true if you want to see what and when is dynamically loaded
            debug: false
        });
        
        $stateProvider 
            .state('index', {
                abstract: true,
                url: "/",
                templateUrl: "views/common/content.html",
                resolve: {
                    CHECK_LOGIN : ['LoginService', function(LoginService){
                        return LoginService.checkLogin();
                    }]
                }
            })
            .state('index.main', {
                url: "dashboard",
                templateUrl: "views/main.html",
                data: { pageTitle: 'Example view' },
                controller : 'LandingCtrl as vm'
            });
        

    }
})();
 