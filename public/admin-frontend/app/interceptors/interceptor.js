(function () {
    'use strict';

    angular
       .module('main')
       .factory('authInterceptor', authInterceptor);

    function authInterceptor($q, $injector, SessionService, $rootScope) {

        var allowedStates =['login'];
        
        var ResponseData = {
            'request': function (config, data) {

                config.headers = config.headers || {};

                //include token only when it is api call not the angularjs templates...
                if ( config.url && config.url.indexOf('.html') == -1 && SessionService.getLoginStatus() )
                {
                    config.headers.Authorization = 'Bearer ' + SessionService.getToken();                

                }

                return config;
            },

            'response': function (res) {
                
               
                var toaster = $injector.get('toaster');

                if ( res && res.data && res.data.status && res.data.type && res.data.status == 'success' && res.data.type == 'single-message' )
                {
                                   
                    if ( res.data.successMessage )
                    {
                      toaster.pop({
                            type: 'success',
                            title: 'Success',
                            body: res.data.successMessage,
                            showCloseButton: true,
                            timeout: 1500
                      });  
                    }
                   
                }
                else
                {
                    if ( res && res.data && res.data.status && res.data.type && res.data.status == 'fail' && res.data.type == 'single-message')
                    {   
                        if ( res.data.errorMessage )
                        {
                            toaster.pop({
                                type: 'error',
                                title: 'Fail',
                                body: res.data.errorMessage,
                                showCloseButton: true,
                                timeout: 1500
                            });
                        }

                    }

                }

               
                
                return $q.resolve(res);
            },

            'responseError': function (res) {
                            
                if ( res.data && res.data.type == 'auth' )
                {
                    
                    SessionService.clearUserSession();
                    var state = $injector.get('$state');
                    var toaster = $injector.get('toaster');
                    if( allowedStates.indexOf(state.current.name) == -1 )
                    {
                       SessionService.loginPage(state);
                       toaster.pop({
                                type: 'error',
                                title: 'Fail',
                                body: 'Session Expired.',
                                showCloseButton: true,
                                timeout: 1500
                            });
                       
                    }
                }   

                return $q.reject(res);
            }
        }
        return ResponseData;
    };

})();


(function () {
    'use strict';

    angular
       .module('main')
       .config(['$httpProvider', function interceptorsPusher($httpProvider) {
            $httpProvider.interceptors.push('authInterceptor');
        }]);

})();

