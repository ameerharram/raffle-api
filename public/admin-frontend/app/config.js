var　basePath = "http://localhost:8000/api/admin/";
angular.module("config", [])
    .constant("CHECK_LOGIN", basePath + "check-login")
    .constant("USER_LOGIN", basePath + "login")
    .constant("USER_LOGOUT", basePath + "logout")
    .constant("USER_PROFILE", basePath + "profile")
    .constant("USER_PROFILE_UPDATE", basePath + "profile-update")
    .constant("USER_CHANGE_PASSWORD", basePath + "profile-change-password")
    .constant("GET_TOKEN", basePath + "token")
    .constant("REFRESH_TOKEN", basePath + "token/refresh")
    .constant("DASHBOARD_STATS", basePath + "dashboard-stats")
    .constant("USERS", basePath + "users")
    .constant("LOTTERIES", basePath + "lotteries")
    .constant("TICKETS", basePath + "tickets")
    .constant("PARTICIPANTS", basePath + "participants")
    .constant("IMPORT_ORDERS", basePath + "import-orders")
    ;
    
  
    

