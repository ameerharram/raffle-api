var　basePath = "http://localhost:8000/api/admin/";
angular.module("config", [])
    .constant("CHECK_LOGIN", basePath + "check-login")
    .constant("USER_LOGIN", basePath + "login")
    .constant("USER_LOGOUT", basePath + "logout")
    .constant("USER_PROFILE", basePath + "profile")
    .constant("USER_PROFILE_UPDATE", basePath + "profile-update")
    .constant("USER_CHANGE_PASSWORD", basePath + "profile-change-password")
    .constant("GET_TOKEN", basePath + "token")
    .constant("REFRESH_TOKEN", basePath + "token/refresh")
    .constant("DASHBOARD_STATS", basePath + "dashboard-stats")
    .constant("USERS", basePath + "users")
    .constant("LOTTERIES", basePath + "lotteries")
    .constant("TICKETS", basePath + "tickets")
    .constant("PARTICIPANTS", basePath + "participants")
    .constant("IMPORT_ORDERS", basePath + "import-orders")
    ;
    
  
    


/**
 * INSPINIA - Responsive Admin Theme
 * 2.6.2
 *
 * Custom scripts
 */

$(document).ready(function () {


    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeight = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if(navbarHeight > wrapperHeigh){
            $('#page-wrapper').css("min-height", navbarHeight + "px");
        }

        if(navbarHeight < wrapperHeigh){
            $('#page-wrapper').css("min-height", $(window).height()  + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            if (navbarHeight > wrapperHeigh) {
                $('#page-wrapper').css("min-height", navbarHeight  + "px");
            } else {
                $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
            }
        }

    }

    $(window).bind("load resize scroll", function() {
        if(!$("body").hasClass('body-small')) {
                fix_height();
        }
    });

    // Move right sidebar top after scroll
    $(window).scroll(function(){
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav') ) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    setTimeout(function(){
        fix_height();
    });

});

// Minimalize menu when screen is less than 768px
$(window).bind("load resize", function () {
    if ($(document).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});

(function () {
    'use strict';

    angular
        .module('main', [
            'ui.router',
            'ui.bootstrap',
            'oc.lazyLoad',
            'toaster',
            'ladda',
            'config',
            'datatables',
            'vesparny.fancyModal',
            //  'oitozero.ngSweetAlert',
            /*Custom Modules*/
            'login',
            'users',
            'lotteries',
            'tickets',
            'participants',
            'settings',
            'api'
        ])
        .config(config)
        .run();

    config.$inject = ['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider'];
    function config($stateProvider,$urlRouterProvider,$ocLazyLoadProvider) {
        $urlRouterProvider.otherwise('/dashboard');

        $ocLazyLoadProvider.config({
            //Set to true if you want to see what and when is dynamically loaded
            debug: false
        });
        
        $stateProvider 
            .state('index', {
                abstract: true,
                url: "/",
                templateUrl: "views/common/content.html",
                resolve: {
                    CHECK_LOGIN : ['LoginService', function(LoginService){
                        return LoginService.checkLogin();
                    }]
                }
            })
            .state('index.main', {
                url: "dashboard",
                templateUrl: "views/main.html",
                data: { pageTitle: 'Example view' },
                controller : 'LandingCtrl as vm'
            });
        

    }
})();
 
(function () {
    'use strict';

    angular
       .module('main')
       .factory('authInterceptor', authInterceptor);

    function authInterceptor($q, $injector, SessionService, $rootScope) {

        var allowedStates =['login'];
        
        var ResponseData = {
            'request': function (config, data) {

                config.headers = config.headers || {};

                //include token only when it is api call not the angularjs templates...
                if ( config.url && config.url.indexOf('.html') == -1 && SessionService.getLoginStatus() )
                {
                    config.headers.Authorization = 'Bearer ' + SessionService.getToken();                

                }

                return config;
            },

            'response': function (res) {
                
               
                var toaster = $injector.get('toaster');

                if ( res && res.data && res.data.status && res.data.type && res.data.status == 'success' && res.data.type == 'single-message' )
                {
                                   
                    if ( res.data.successMessage )
                    {
                      toaster.pop({
                            type: 'success',
                            title: 'Success',
                            body: res.data.successMessage,
                            showCloseButton: true,
                            timeout: 1500
                      });  
                    }
                   
                }
                else
                {
                    if ( res && res.data && res.data.status && res.data.type && res.data.status == 'fail' && res.data.type == 'single-message')
                    {   
                        if ( res.data.errorMessage )
                        {
                            toaster.pop({
                                type: 'error',
                                title: 'Fail',
                                body: res.data.errorMessage,
                                showCloseButton: true,
                                timeout: 1500
                            });
                        }

                    }

                }

               
                
                return $q.resolve(res);
            },

            'responseError': function (res) {
                            
                if ( res.data && res.data.type == 'auth' )
                {
                    
                    SessionService.clearUserSession();
                    var state = $injector.get('$state');
                    var toaster = $injector.get('toaster');
                    if( allowedStates.indexOf(state.current.name) == -1 )
                    {
                       SessionService.loginPage(state);
                       toaster.pop({
                                type: 'error',
                                title: 'Fail',
                                body: 'Session Expired.',
                                showCloseButton: true,
                                timeout: 1500
                            });
                       
                    }
                }   

                return $q.reject(res);
            }
        }
        return ResponseData;
    };

})();


(function () {
    'use strict';

    angular
       .module('main')
       .config(['$httpProvider', function interceptorsPusher($httpProvider) {
            $httpProvider.interceptors.push('authInterceptor');
        }]);

})();


/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
(function () {
    'use strict';
    angular
        .module('main')
        .directive('iboxTools', iboxTools);

    function iboxTools($timeout) {
        return {
            restrict: 'A',
            scope: true,
            templateUrl: 'views/common/ibox_tools.html',
            controller: function ($scope, $element) {
                // Function for collapse ibox
                $scope.showhide = function () {
                    var ibox = $element.closest('div.ibox');
                    var icon = $element.find('i:first');
                    var content = ibox.children('.ibox-content');
                    content.slideToggle(200);
                    // Toggle icon from up to down
                    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    $timeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                },
                    // Function for close ibox
                    $scope.closebox = function () {
                        var ibox = $element.closest('div.ibox');
                        ibox.remove();
                    }
            }
        };
    }
})();
    
/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
(function () {
    'use strict';
    angular
        .module('main')
        .directive('iboxToolsFullScreen', iboxToolsFullScreen);

    function iboxToolsFullScreen($timeout) {
        return {
            restrict: 'A',
            scope: true,
            templateUrl: 'views/common/ibox_tools_full_screen.html',
            controller: function ($scope, $element) {
                // Function for collapse ibox
                $scope.showhide = function () {
                    var ibox = $element.closest('div.ibox');
                    var icon = $element.find('i:first');
                    var content = ibox.children('.ibox-content');
                    content.slideToggle(200);
                    // Toggle icon from up to down
                    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    $timeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                };
                // Function for close ibox
                $scope.closebox = function () {
                    var ibox = $element.closest('div.ibox');
                    ibox.remove();
                };
                // Function for full screen
                $scope.fullscreen = function () {
                    var ibox = $element.closest('div.ibox');
                    var button = $element.find('i.fa-expand');
                    $('body').toggleClass('fullscreen-ibox-mode');
                    button.toggleClass('fa-expand').toggleClass('fa-compress');
                    ibox.toggleClass('fullscreen');
                    setTimeout(function() {
                        $(window).trigger('resize');
                    }, 100);
                }
            }
        };
    }
})();
/**
 * minimalizaSidebar - Directive for minimalize sidebar
*/
(function () {
    'use strict';
    angular
        .module('main')
        .directive('minimalizaSidebar', minimalizaSidebar);

    function minimalizaSidebar($timeout) {
        return {
            restrict: 'A',
            template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
            controller: function ($scope, $element) {
                $scope.minimalize = function () {
                    $("body").toggleClass("mini-navbar");
                    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                        // Hide menu in order to smoothly turn on when maximize menu
                        $('#side-menu').hide();
                        // For smoothly turn on menu
                        setTimeout(
                            function () {
                                $('#side-menu').fadeIn(400);
                            }, 200);
                    } else if ($('body').hasClass('fixed-sidebar')){
                        $('#side-menu').hide();
                        setTimeout(
                            function () {
                                $('#side-menu').fadeIn(400);
                            }, 100);
                    } else {
                        // Remove all inline style from jquery fadeIn function to reset menu state
                        $('#side-menu').removeAttr('style');
                    }
                }
            }
        };
    }
})();
/**
 * pageTitle - Directive for set Page title - mata title
 */
(function () {
    'use strict';
    angular
        .module('main')
        .directive('pageTitle', pageTitle);

    function pageTitle($rootScope, $timeout) {
        return {
            link: function(scope, element) {
                var listener = function(event, toState, toParams, fromState, fromParams) {
                    // Default title - load on Dashboard 1
                    var title = 'Charity Moose | Raffle';
                    // Create your own title pattern
                    if (toState.data && toState.data.pageTitle) title = 'Charity Moose | ' + toState.data.pageTitle;
                    $timeout(function() {
                        element.text(title);
                    });
                };
                $rootScope.$on('$stateChangeStart', listener);
            }
        }
    }

})();
/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
 (function () {
    'use strict';
    angular
        .module('main')
        .directive('sideNavigation', sideNavigation);

    function sideNavigation($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element) {
                // Call the metsiMenu plugin and plug it to sidebar navigation
                $timeout(function(){
                    element.metisMenu();

                });

                // Colapse menu in mobile mode after click on element
                var menuElement = $('#side-menu a:not([href$="\\#"])');
                menuElement.click(function(){
                    if ($(window).width() < 769) {
                        $("body").toggleClass("mini-navbar");
                    }
                });

                // Enable initial fixed sidebar
                if ($("body").hasClass('fixed-sidebar')) {
                    var sidebar = element.parent();
                    sidebar.slimScroll({
                        height: '100%',
                        railOpacity: 0.9,
                    });
                }
            }
        };
    }

})();

(function () {
    'use strict';

    angular
       .module('main')
       .service('Common', Common);


    function Common($state, SessionService, UsersService){
        this.getUser = function(user)
        {


            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                   user(res.data.responseData);
                   
                    
                }else
                {
                    user(null);
                }
            }

            function failed(res)
            {
                user(null);
            }

            UsersService.
                        getLoggedInUser()
                        .then(success,failed);
        };

        this.logout = function()
        {
             function success(res){
                
                   SessionService.clearUserSession();
                
            }

            function failed(res)
            {
                SessionService.clearUserSession();
                SessionService.loginPage($state);
            }

            UsersService.
                        logout()
                        .then(success,failed).finally(function(){
                            SessionService.clearUserSession();
                            SessionService.loginPage($state);
                        });
            
        }
    }



})();




(function () {
    'use strict';

    angular
       .module('main')
       .service('SessionService', SessionService);

    function SessionService($rootScope){

        this.setToken = function(token){
            this.setCookie('_token', token);
        }

        this.getToken = function(){
            return this.getCookie('_token');
        }

        this.getLoginStatus = function(){
            return this.getCookie('_token') ? true : null;
        };

        this.setRememberMeFlag = function(){
            this.setCookie('remember_me_flag', true);
        };

        this.getRememberMeFlag = function(){
            return this.getCookie('remember_me_flag');
        };

        this.removeRememberMeFlag = function(){
            this.delete_cookie('remember_me_flag');
        };

        this.clearUserSession = function(){
            this.delete_cookie('_token');
            this.delete_cookie('remember_me_flag');
            localStorage.clear();
            sessionStorage.clear();
        };

        this.loginPage = function(state){
           
            state.go('login');

        }

        this.dashboardPage = function(state){
            state.go('index.main');
        }

        this.getCookie = function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return null;
        };

        this.setCookie = function(cname, cvalue) {
            var d = new Date();
            d.setTime(d.getTime() + (60*24*60*60*1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
        };

        this.setSessionCookie = function(cname, cvalue) {
            var expires = "expires=0";
            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
        };

        this.delete_cookie = function(name) {
            document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        };
    }
})();
(function () {
    'use strict';

    angular
        .module('main')
        .controller('ApiKeyCtrl', ApiKeyCtrl);

    function ApiKeyCtrl($rootScope, toaster , ApiService) {

    	/*Properties*/
        var vm = this;
        vm.token = null;
        vm.loading = null;
        
        /*Methods*/
        vm.getApiKey = getApiKey;
        vm.refreshToken = refreshToken;
        vm.copied = copied;

        init();

        function getApiKey()
        {

        	function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.token  = res.data.responseData.token;
                    
                }
            }

            function failed(res)
            {

            }

            ApiService.
                         getApiKey()
                        .then(success,failed).finally(function() {  });
        }

        function refreshToken()
        {
            vm.loading = true;

            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.token  = res.data.responseData.token;
                    
                }
            }

            function failed(res)
            {

            }

            ApiService.
                         getRefreshKey()
                        .then(success,failed).finally(function() { vm.loading = false });
        }

     	function init()
        {
            vm.getApiKey();
        }

        function copied() 
        {
            toaster.pop({
                        type: 'success',
                        title: 'API KEY',
                        body: 'Copied successfully.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

       
	}
})();
 
(function () {
    'use strict'; 

    angular
        .module('api', ['ngclipboard'])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('api', {
                url: "/",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('api.token', {
                url: "api/refresh-token",
                templateUrl: "views/api/refresh_token.html",
                data: { pageTitle: 'Create Lottery' },
                controller : "ApiKeyCtrl as vm"
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('ApiService', function (
                                              $http,
                                              GET_TOKEN,
                                              REFRESH_TOKEN
                                                   
                                             ) {

            this.getApiKey = function ( ) {
                return $http.get( GET_TOKEN );
            };

            this.getRefreshKey = function ( ) {
                return $http.get( REFRESH_TOKEN );
            };

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .controller('LandingCtrl', LandingCtrl);

    function LandingCtrl($rootScope, LandingService) {

    	/*Properties*/
        var vm = this;
        vm.stats =  null;
        /*Methods*/
        vm.getDashboardStats = getDashboardStats;

        init();

        function getDashboardStats()
        {

        	function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.stats  = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            LandingService.
                        getDashboardStats()
                        .then(success,failed).finally(function() {  });
        }

     	function init()
        {
            vm.getDashboardStats();
        }

       
	}
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .service('LandingService', function (
                                              $http,
                                              DASHBOARD_STATS
                                                   
                                             ) {

            this.getDashboardStats = function ( ) {
                return $http.get( DASHBOARD_STATS );
            };

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .controller('LoginCtrl', LoginCtrl);

    function LoginCtrl($scope, $state, toaster ,LoginService ,SessionService ,CHECK_LOGIN) {
        
        /*Resolver Checks*/
        if(CHECK_LOGIN)
        {
            SessionService.dashboardPage($state);
            toaster.pop({
                        type: 'warning',
                        title: 'Fail',
                        body: 'Already Logged In.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }
        

        /*Properties*/
        var vm = this;
        vm.credentials =  null;
        vm.loading = false;
        
        /*Methods*/
        vm.login = login;
        





        /*Methods Implememntation*/
        function login(isValid)
        {
            if(!isValid) return false; // breaks function execution
            
            vm.loading = true;

            function success(res) {
                if ( res && res.data && res.data.status && res.data.type && res.data.status == 'success' && res.data.type == 'single-message' )
                {
                    SessionService.setToken(res.data.responseData.token);
                    SessionService.dashboardPage($state);                                       
                    console.log('its success');
                }
            }

            function failed(response) {
                // Notification.error(response.data.message);
            }

            LoginService
                        .login(vm.credentials)
                        .then(success,failed).finally(function() { vm.loading = false; });

        }
	}
})();
(function () {
    'use strict'; 

    angular
        .module('login', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('login', {
                url: "/login",
                templateUrl: "views/login.html",
                data: { pageTitle: 'Login view' },
                controller : 'LoginCtrl as vm',
                resolve: {
                    CHECK_LOGIN: ['$state','toaster','SessionService', function ($state,toaster,SessionService) {
                        return SessionService.getLoginStatus();
                        
                    }]
                }
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('LoginService', function ($http,
                                                    $q,
                                                    $rootScope,
                                                    USER_LOGIN,
                                                    CHECK_LOGIN
                                                    ) {

            this.login = function (credentials) {
                return $http.post(USER_LOGIN , credentials);
            };

            this.checkLogin = function () {
                return $http.get(CHECK_LOGIN );
            };


        });  
})();  
(function () {
    'use strict'; 

    angular
        .module('lotteries', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('lotteries', {
                url: "/raffles",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('lotteries.create', {
                url: "/create",
                templateUrl: "views/lotteries/create_lottery.html",
                data: { pageTitle: 'Create Lottery' },
                controller : "LotteriesCreateCtrl as vm"
            })
           .state('lotteries.list', {
                url: "/list",
                templateUrl: "views/lotteries/lotteries_list.html",
                data: { pageTitle: 'Lotteries List' },
                controller : "LotteriesListCtrl as vm"
            })
           .state('lotteries.draw_winner', {
                url: "/:lottery_id/draw-winner",
                templateUrl: "views/lotteries/draw_winner.html",
                data: { pageTitle: 'Draw Winner' },
                controller : "DrawWinnerCtrl as vm"
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('LotteriesService', function ( $http,
                                                LOTTERIES         
                                           ) {

            this.getLotteries = function() {
                return $http.get(LOTTERIES);
            };

            this.createLottery = function(lottery) {
                return $http.post(LOTTERIES, lottery);
            };

            this.getLotteryWinner = function(lottery_id ,drawCount) {
                return $http.get(LOTTERIES +'/'+ lottery_id + '/'+ drawCount +'/draw-winner');
            };

            this.saveDrawResult = function(winner) {
                return $http.post(LOTTERIES +'/save-draw-result' ,winner);
            };

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .controller('MainCtrl', MainCtrl);

    function MainCtrl() {
        this.userName = 'Example user';
	    this.helloText = 'Welcome in SeedProject';
	    this.descriptionText = 'It is an application skeleton for a typical AngularJS web app. You can use it to quickly bootstrap your angular webapp projects and dev environment for these projects.';
	    console.log('Main Controller....');
	}
})();
(function () {
    'use strict';

    angular
        .module('main')
        .controller('NavBarCtrl', NavBarCtrl);

    function NavBarCtrl($scope , Common){

        /*Properties*/
        var vm = this;
        vm.user = null;
        vm.contentLoader = false;
        /*Methods*/
        $scope.logout = Common.logout;

        Common.getUser(function(user){
            if ( user )
            {
                $scope.user = user;
                $scope.user.userName = $scope.user.first_name +' '+ $scope.user.last_name;
            }
        });

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('TopNavBar', TopNavBar);

    function TopNavBar($scope , Common){

        /*Properties*/
        var vm = this;
        /*Methods*/
        $scope.logout = Common.logout;
    }
})();
 


(function () {
    'use strict'; 

    angular
        .module('participants', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('participants', {
                url: "/participants",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('participants.create', {
                url: "/create",
                templateUrl: "views/participants/create_participant.html",
                data: { pageTitle: 'Create Participant' },
                controller : 'ParticipantsCreateCtrl as vm'
                
            })
           .state('participants.create-ticket', {
                url: "/:participant_id/create-tickets",
                templateUrl: "views/participants/create_participant_tickets.html",
                data: { pageTitle: 'Create Ticket' },
                controller : 'ParticipantsCreateCtrl as vm'
                
            })
           .state('participants.list', {
                url: "/list",
                templateUrl: "views/participants/participants_list.html",
                data: { pageTitle: 'Participants List' },
                controller : 'ParticipantsListCtrl as vm'
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('ParticipantsService', function ( $http,
                                                PARTICIPANTS,
                                                TICKETS         
                                           ) {

            this.getParticipants = function() {
                return $http.get(PARTICIPANTS);
            };

            this.createParticipant = function( participant ) {
                return $http.post( PARTICIPANTS , participant );
            };

            this.createParticipantTickets = function( tickets ) {
                return $http.post( TICKETS , tickets );
            };


        });  
})();  
(function () {
    'use strict'; 

    angular
        .module('settings', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('settings', {
                url: "/settings",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('settings.import', {
                url: "/import",
                templateUrl: "views/settings/import.html",
                data: { pageTitle: 'Import Data' },
                controller : "ImportCtrl as vm"
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('SettingsService', function ( $http,
                                                IMPORT_ORDERS         
                                           ) {

            this.importOrders = function( dates ) {
                return $http.post(IMPORT_ORDERS , dates );
            };

        });  
})();  
(function () {
    'use strict'; 

    angular
        .module('tickets', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('tickets', {
                url: "/tickets",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('tickets.create', {
                url: "/create",
                templateUrl: "views/tickets/create_ticket.html",
                data: { pageTitle: 'Create Lottery' }
            })
           .state('tickets.list', {
                url: "/list",
                templateUrl: "views/tickets/tickets_list.html",
                data: { pageTitle: 'tickets List' },
                controller : 'TicketsListCtrl as vm'
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('TicketsService', function ( $http,
                                                TICKETS         
                                           ) {

            this.getTickets = function() {
                return $http.get(TICKETS);
            };


        });  
})();  
(function () {
    'use strict'; 

    angular
        .module('users', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('users', {
                url: "/users",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('users.create', {
                url: "/create",
                templateUrl: "views/users/create_user.html",
                data: { pageTitle: 'Create User' },
                controller : "UsersCreateCtrl as vm"
            })
           .state('users.list', {
                url: "/list",
                templateUrl: "views/users/user_list.html",
                data: { pageTitle: 'Users List' },
                controller : "UsersListCtrl as vm"
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('UsersService', function ($http,
                                           USERS,
                                           USER_PROFILE,
                                           USER_LOGOUT        
                                           ) {

            this.getUsers = function() {
                return $http.get(USERS);
            };

            this.createUser = function(user) {
                return $http.post(USERS, user);
            };


            this.getLoggedInUser = function () {
                return $http.get(USER_PROFILE);

            }
            this.logout = function () {
                return $http.get(USER_LOGOUT );

            }


        });  
})();  
(function () {
    'use strict'; 

    angular
        .module('users', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $stateProvider
           .state('users', {
                url: "/users",
                templateUrl: "views/common/content.html",
                abstract: true
            })
           .state('users.create', {
                url: "/create",
                templateUrl: "views/users/create_user.html",
                data: { pageTitle: 'Create User' },
                controller : "UsersCreateCtrl as vm"
            })
           .state('users.list', {
                url: "/list",
                templateUrl: "views/users/user_list.html",
                data: { pageTitle: 'Users List' },
                controller : "UsersListCtrl as vm"
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .controller('LotteriesCreateCtrl', LotteriesCreateCtrl);

    function LotteriesCreateCtrl($scope  , $filter, LotteriesService){

        /*Properties*/
        var vm = this;
        vm.lottery = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.createLottery = createLottery;
        vm.reset = reset;

        function createLottery()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.lottery = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

            var data = angular.copy(vm.lottery);

            if(data && data.start)
            {
                data.start = $filter('date')(data.start ,'yyyy-MM-dd');
            }

            if(data && data.end)
            {
               data.end = $filter('date')(data.end ,'yyyy-MM-dd' ); 
            }

            if(data && data.announcment)
            {
                data.announcment = $filter('date')(data.announcment ,'yyyy-MM-dd' );
            }
            
            LotteriesService.
                        createLottery(data)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        
        }
        function reset() 
        {
            vm.lottery = null;
            vm.errors = null;
            vm.loading = null;
        }    


        
    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('DrawWinnerCtrl', DrawWinnerCtrl);

    function DrawWinnerCtrl($rootScope,$scope , $stateParams, $fancyModal, LotteriesService) {

    	/*Properties*/
        var vm = this;
        vm.lottery_id =  $stateParams.lottery_id;
        vm.participants =  null;
        vm.winner = null;
        vm.btnLoader =  false;
        vm.savebtnLoader =  false;
        vm.drawCount = 0;

        /*Methods*/
        vm.getLotteryWinner = getLotteryWinner;
        vm.saveDrawResult = saveDrawResult;
        vm.winnerPop = winnerPop;

        init();

        function getLotteryWinner()
        {
            vm.btnLoader = true;
            vm.participants =  null;
            vm.winner = null;
        	function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.participants = res.data.responseData.participants;
                    vm.winner = res.data.responseData.winner;
                    $scope.winner = vm.winner;
                    setTimeout(function(){
                        vm.winnerPop();
                    },1000);

                    
                }
            }

            function failed(res)
            {

            }
            vm.drawCount = vm.drawCount + 1;
            LotteriesService.
                        getLotteryWinner(vm.lottery_id , vm.drawCount )
                        .then(success,failed).finally(function() { vm.btnLoader = false;  });
        }

        function saveDrawResult() 
        {
            vm.savebtnLoader = true;
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                                     
                }
            }

            function failed(res)
            {

            }

            LotteriesService.
                        saveDrawResult(vm.winner)
                        .then(success,failed).finally(function() { vm.savebtnLoader = false;  });
        }

     	function init()
        {
            vm.getLotteryWinner();
        }

        function winnerPop() {
            $fancyModal.open({ templateUrl: 'views/modals/winner.html', scope:$scope });
        };


       
	}
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('LotteriesListCtrl', LotteriesListCtrl);

    function LotteriesListCtrl($scope ,toaster , $fancyModal , DTOptionsBuilder , LotteriesService ){

        /*Properties*/
        var vm = this;
        vm.lotteries = null;
        vm.contentLoader = false;
        
        /*Methods*/
        vm.getLotteries = getLotteries;
        vm.commingSoong = commingSoong;
        vm.demo = demo;

        init();


        function getLotteries()
        {
            vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.lotteries = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            LotteriesService.
                        getLotteries()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }

        function init()
        {
            vm.getLotteries();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

        function demo() {
            $fancyModal.open({ templateUrl: 'views/modals/winner.html' });
        };

        

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('ParticipantsListCtrl', ParticipantsListCtrl);

    function ParticipantsListCtrl($scope ,toaster , DTOptionsBuilder , ParticipantsService){

        /*Properties*/
        var vm = this;
        vm.participants = null;
        vm.contentLoader = false;
        
        /*Methods*/
        vm.getParticipants = getParticipants;
        vm.commingSoong = commingSoong;

        init();


        function getParticipants()
        {
            vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.participants = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            ParticipantsService.
                        getParticipants()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }

        function init()
        {
            vm.getParticipants();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('ParticipantsCreateCtrl', ParticipantsCreateCtrl);

    function ParticipantsCreateCtrl($scope  , $filter, $stateParams , ParticipantsService){

        /*Properties*/
        var vm = this;
        vm.participant = null;
        vm.number_of_tickets = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.createParticipant = createParticipant;
        vm.createParticipantTickets = createParticipantTickets;
        vm.reset = reset;

        function createParticipant()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.participant = null;   
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

                       
            ParticipantsService.
                        createParticipant(vm.participant)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        
        }

        function createParticipantTickets()
        {
            vm.loading = true;
            //vm.number_of_tickets = $stateParams.participant_id; 
            var data = {'participant_id' : $stateParams.participant_id, 'number_of_tickets' : vm.number_of_tickets  };
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.number_of_tickets = null;   
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

                       
            ParticipantsService.
                        createParticipantTickets(data)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        
        }
        function reset() 
        {
            vm.participant = null;
            vm.errors = null;
            vm.loading = null;
            vm.number_of_tickets = null;   
        }    


        
    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('ImportCtrl', ImportCtrl);

    function ImportCtrl($scope ,toaster , SettingsService ){

        /*Properties*/
        var vm = this;
        vm.dates = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.importOrders = importOrders;
        vm.reset = reset;

        function importOrders()
        {
            vm.loading = true;
            vm.errors = null;
            function success(res){

                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.dates = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }

            function failed(res)
            {

            }

            SettingsService.
                        importOrders(vm.dates)
                        .then(success,failed).finally(function() { vm.loading = false; });;

        }

        function reset() 
        {
            vm.dates = null;
            vm.errors = null;
            vm.loading = null;
        }  
        

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('TicketsListCtrl', TicketsListCtrl);

    function TicketsListCtrl($scope ,toaster , DTOptionsBuilder , TicketsService){

        /*Properties*/
        var vm = this;
        vm.tickets = null;
        vm.contentLoader = false;
        
        /*Methods*/
        vm.getTickets = getTickets;
        vm.commingSoong = commingSoong;

        init();


        function getTickets()
        {
            vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.tickets = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            TicketsService.
                        getTickets()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }


        function getRefreshKey()
        {
           vm.contentLoader = true;
            
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.tickets = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            TicketsService.
                        getTickets()
                        .then(success,failed).finally(function() { vm.contentLoader = false; }); 
        }

        function init()
        {
            vm.getTickets();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

      

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('UsersCreateCtrl', UsersCreateCtrl);

    function UsersCreateCtrl($scope  , UsersService){

        /*Properties*/
        var vm = this;
        vm.user = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.createUser = createUser;
        vm.resetUser = resetUser;

        function createUser()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.user = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

            UsersService.
                        createUser(vm.user)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        }

        function resetUser() 
        {
            vm.user = null;
            vm.errors = null;
            vm.loading = false;
        }    


        
    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('UsersEditCtrl', UsersEditCtrl);

    function UsersEditCtrl($scope , UsersService){

        /*Properties*/
        var vm = this;
        vm.user = null;
        vm.errors = null;
        vm.loading = null;
        /*Methods*/
        vm.editUser = editUser;
        vm.resetUser = resetUser;

        function editUser()
        {
            vm.loading = true;
            
            function success(res){

                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.user = null;
                    vm.errors = null;
                }
                else if(res && res.data && res.data.status && res.data.status == 'fail')
                {
                    vm.errors = res.data.responseData;    
                }
                
            }
            function failed(res)
            {

            }

            UsersService.
                        editUser(vm.user)
                        .then(success,failed).finally(function() { vm.loading = false; });;
        }

        function resetUser() 
        {
            vm.user = null;
            vm.errors = null;
            vm.loading = false;
        }    


        
    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('UsersListCtrl', UsersListCtrl);

    function UsersListCtrl($scope ,toaster , DTOptionsBuilder , UsersService){

        /*Properties*/
        var vm = this;
        vm.users = null;
        
        /*Methods*/
        vm.getUsers = getUsers;
        vm.commingSoong = commingSoong;

        init();


        function getUsers()
        {
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.users = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            UsersService.
                        getUsers()
                        .then(success,failed).finally(function() {  });;
        }

        function init()
        {
            vm.getUsers();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

    }
   
})();
 


(function () {
    'use strict';

    angular
        .module('main')
        .controller('UsersListCtrl', UsersListCtrl);

    function UsersListCtrl($scope ,toaster , DTOptionsBuilder , UsersService){

        /*Properties*/
        var vm = this;
        vm.users = null;
        vm.contentLoader = false;
        /*Methods*/
        vm.getUsers = getUsers;
        vm.commingSoong = commingSoong;

        init();


        function getUsers()
        {
            vm.contentLoader = true;
            function success(res){
                
                if ( res && res.data && res.data.status && res.data.status == 'success'  )
                {
                    vm.users = res.data.responseData;
                    
                }
            }

            function failed(res)
            {

            }

            UsersService.
                        getUsers()
                        .then(success,failed).finally(function() { vm.contentLoader = false; });;
        }

        function init()
        {
            vm.getUsers();
        }

        function commingSoong() 
        {
            toaster.pop({
                        type: 'warning',
                        title: 'Working',
                        body: 'Functionality under construction.',
                        showCloseButton: true,
                        timeout: 1500
                    });
        }

    }
   
})();
 

