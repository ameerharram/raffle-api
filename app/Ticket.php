<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $guarded = [];

    /*Rules for User*/
    
    public static $CREATE_CHECKOUT_TICKET_RULES = [
        'email' => 'required|email',
        'payment' => 'required|numeric|min:1',
        'wp_user_id' => 'required|numeric',
        'lottery_id' => 'required|numeric|exists:lotteries,id',
        'name' => 'nullable|string|max:300',
        'order_number' => 'required|numeric|unique:tickets,order_number'
    ];

    public static $CREATE_MANUAL_TICKETS_RULES = [
        'number_of_tickets' => 'required|numeric|min:1',
        'participant_id' => 'required|exists:participants,id' 
    ];

    public function participant()
    {
        return $this->belongsTo( 'App\Participant', 'participant_id' );
    }

    public function lottery()
    {
        return $this->belongsTo( 'App\Lottery', 'lottery_id' );
    }
}
