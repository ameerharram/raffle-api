<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Hash;
use DB;
use Mail;
/*Models*/
use App\User;
use Carbon\Carbon;
use App\Mail\SignUp;

use JWTAuth;
use Response;
class AuthController extends ApiController
{

	/*Logout*/	
	public function deAuthenticate()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return $this->successMessageM('loggedOut');
    }

    /*LOGIN FOR ADMIN*/
    public function loginAdmin(Request $request)
    {
    	$data = $request->all();
    	$rules = User::$loginRules;

		/* break point if invalidates */
		$validtor = $this->validateInputs($data ,$rules);
		if($validtor) return $validtor;
		
		/* Follow will executed if no break occur */
		$user = User::where( ['email' => $data['email'], 'user_type' => User::$ADMIN_KEY ])
					->first();
		if( $user && Hash::check($data['password'],$user->password) )
		{	
			$customClaims = [];
			// attempt to verify the credentials and create a token for the user
            if ( !$token = JWTAuth::fromUser($user, $customClaims) )
            {
				return $this->errorMessageM('wrongCredentials');
            }
            
            return $this->responseDataM(['token' => $token , 'user_type' => $user->user_type ],'loggedIn');
			
		}
		return $this->errorMessageM('wrongCredentials');
	}


	public function refreshToken()
	{

		$api = User::where('email','api')->first();

		/* Follow will executed if no break occur */
		
		if( $api )
		{	
			$customClaims = [];
			// attempt to verify the credentials and create a token for the user
            if ( !$token = JWTAuth::fromUser($api, $customClaims) )
            {
				return $this->errorMessageM('wrongCredentials');
            }
            
			$api->password = $token;
			$api->save();

            return $this->responseDataM( ['token' => $token], 'recordUpdateSuccess' );
			
		}
		return $this->errorMessage('wrongCredentials');

	}

	public function getApiToken()
	{

		$api = User::where('email','api')->first();
		$data['token'] = $api->password;
		return $this->responseData($data);	
	}
	


	public function changePassword(Request $request)
	{
		$data = $request->all();
		$rules = User::$updatePassword;

		/* break point if invalidates */
		$validtor = $this->validateInputs($data ,$rules);
		if($validtor) return $validtor;

		$user = Auth()->user();

		if( !Hash::check($data['old_password'],$user->password) )
			return $this->errorMessageM('invalidPassword');
		
		$user->password = Hash::make($data['new_password']);
		
		if(!$user->save())
			return $this->errorMessageM('oppFail');

		return $this->successMessageM('passwordUpdated');
	}


	public function checkLogin()
	{
		return Response::json([ 'status' => 'success', 'type' => 'auth', 'successMessage' => 'valid' ]);
	}

}
