<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Lottery;
use App\Ticket;
use App\Participant;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\SettingController;
// use App\Http\Controllers\SettingController;

class PublicApiController extends ApiController
{
    //
    public function createTicketFromCheckout ( Request $request )
    {
        $data = $request->all();
        $lottery = Lottery::where('is_active','active')->first();
        if( !$lottery ){ return $this->errorMessageM('activeLotteryFailed'); }

        $flag = SettingController::processOrder( $data['order_number'] , $lottery  );
        if( !$flag )
        {
           return $this->errorMessage( 'somethingWrong' );
        }
        return $this->successMessage( 'added' );
    } 

    public function syncMemberShipTickets()
    {
    	return $request->all();	
    }


    /*Lottery Detail of Last Active Contest*/
    public function getParticipantLotteryDetails(Request $request)
    {
        $data = $request->all();
    	$rules = Participant::$PARTICIPANT_RULES;

    	$validator = $this->validateInputs( $data, $rules );
    	if( $validator ) return $validator;

		$lottery = Lottery::find( $data['lottery_id'] );
    	$participant = Participant::where( [ 'lottery_id' => $data['lottery_id'] , 'wp_user_id' => $data['wp_user_id']  ] )->first();
    	
        $data = array( 'total_tickets' => 0 , 'participant_tickets' => 0 ,'raffle_ends' => Carbon::parse($lottery->end)->toFormattedDateString());
        //$data['total_tickets'] = count( $lottery->tickets()->get() );
        
        $data['status'] = $lottery->is_active;
        
        if( $participant )
        {
            $data['participant_tickets'] = count( $participant->tickets()->get() ); 
        }
        
        $data['win_status'] = 'pending';
        // return $lottery;
        
        if( $lottery->is_active == 'compeleted' )
        {
           $data['win_status'] = 'winner'; 
           $winnerTick = $lottery->tickets()->where(['is_winner'=>1 ,'participant_id' => $participant->id])->first();
           if( !$winnerTick)  $data['win_status'] = 'lost';
               
        }

        // $data['probablity'] = $data['participant_tickets'];
        $data['probablity'] = 0;
        $data['total_tickets'] = 0;
        // if($data['total_tickets'] != 0 )
        // {
        //     $data['probablity'] = round( ( ( $data['participant_tickets']/ $data['total_tickets']  ) * 100 ) );
        // }

    	return $this->responseData( $data );
    }

    public function getWinnerFromContest(Lottery $lottery)
    {
    	
    	$winnerTick = $lottery->tickets()->where('is_winner',1)->first();
        if( !$winnerTick) return $this->errorMessage('winnerNoTExist');

        $winnerTick->participant;
		
		return $this->responseData( $winnerTick );
	}

}
