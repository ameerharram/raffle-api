<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Response;

class ApiController extends Controller
{
    public function __construct(){
    	//
    }
	public function validateInputs($inputs, $rules, $customeMessages = [])
    {
        $validator = Validator::make($inputs, $rules, $customeMessages);
        if ( $validator->fails() )
             return $this->validationErrors($validator->getMessageBag()->toArray());
            //die($this->validationErrors($validator->getMessageBag()->toArray()));
        return false;    
    }

    public function validationErrors($errors, $msg = 'errorMessage')
    {
        return json_encode([ 'status' => 'fail', 'errorMessage' => trans("api.$msg"), 'responseData' => $errors ]);
    }

    public function validationErrorsM($errors, $msg = 'errorMessage')
    {
        return json_encode([ 'status' => 'fail', 'type' => 'single-message' ,'errorMessage' => trans("api.$msg"), 'responseData' => $errors ]);
    }

    public function errorMessage($msg)
    {
        return Response::json([ 'status' => 'fail', 'errorMessage' => trans("api.$msg") ]);
    }


    public function errorMessageM($msg)
    {
        return Response::json([ 'status' => 'fail', 'type' => 'single-message', 'errorMessage' => trans("api.$msg") ]);
    }


    public function successMessage($msg)
    {
        return Response::json([ 'status' => 'success', 'successMessage' => trans("api.$msg") ]);
    }

    public function successMessageM($msg)
    {
        return Response::json([ 'status' => 'success', 'type' => 'single-message' ,'successMessage' => trans("api.$msg") ]);
    }

    public function responseData($data)
    {
    	return Response::json([ 'status' => 'success', 'responseData' => $data ]);
    }

    public function responseDataM($data,$msg)
    {
        return Response::json([ 'status' => 'success', 'type' => 'single-message', 'responseData' => $data, 'successMessage' => trans("api.$msg") ]);
    }

    public function message($string, $type = null)
    {
        return Response::json([ 'status' => is_null($type) ? 'success' : 'fail', is_null($type) ? 'successMessage' : 'errorMessage' => $string ]);   
    }
    /*NEED TO REMOVE IT IF NOT USED*/
    public function getInstLogoLink($fileName){
        $path = \Config::get('constants.READ_INSTITUTION_LOGOS');
        return  $fileName ? $path . $fileName : null;
    }


    public function imageMove(Request $request , $imageParam , $folderName , $fileName = null){
        
        $folderName = 'constants.'. $folderName;
        
        if ( ($request->hasFile($imageParam) && $request->file($imageParam)->isValid()) )
        {
                
                $file = $request->file($imageParam);
                if(!$fileName){
                    $fileName = setfileName(getExtension($file->getMimeType()));
                }
                
                if ( $file->move( \Config::get($folderName), $fileName) == false )
                {
                    return false;
                }

                return $fileName;
        }

        return false;

    }
}

