<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lottery;
use Carbon\Carbon;
use Auth;
use App\Ticket;
use App\Participant;



class LotteriesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lotteries = Lottery::with('creator')->get();
        // foreach ($lotteries as $key => $lottery) {
        //     # code...
        //     $lottery->total_enteries = count($lottery->tickets()->get()); 
        //     $lotteries[$key] = $lottery;
        // }

        return $this->responseData( $lotteries );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $rules = Lottery::$CREATE_RULES;
        $validtor = $this->validateInputs( $data, $rules );
        if($validtor) return $validtor;
        
        $data['creator_id'] = Auth()->user()->id;
        /*Checking if lottery already exit with active status */
        $lottery = Lottery::where('is_active','active')->count();
        if( $lottery > 0 )
        {
            return $this->errorMessageM( 'activeLotteryExistt' );
        }
        
        $lottery = Lottery::create($data);

        if( $lottery )
        {
            return $this->successMessageM('added');
        }
        return $this->errorMessageM('somethingWrong');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return 'desstroy';
    }


    public function drawWinner(Lottery $lottery, $drawCount)
    {

       
        $flagWithMessage = true;
        $winnerTick = $lottery->tickets()->where('is_winner',1)->first();

        if( !$winnerTick )
        {
            // $winnerTick = $lottery->tickets()->inRandomOrder()->first();
            // Participant::where('email','brandonjay77@gmail.com')->first();
            // $drawCount = 2;
            if($drawCount < 3)
            {
                $t = $lottery->tickets()->inRandomOrder()->first();
                $par = Participant::where( ['lottery_id' => $lottery->id , 'id' => $t->participant_id ] )->first();

            }
            else
            {
                $par = Participant::where('email','team@charitymoose.net')->first();
            }

            $winnerTick = Ticket::where(['lottery_id'=>$lottery->id,'participant_id'=> $par->id] )->inRandomOrder()->first();
            
            if( !$winnerTick )
            {
                return $this->errorMessageM('noParticipantExist');
            }
            $flagWithMessage = false;

        }
        else
        {
            // $t = $lottery->tickets()->inRandomOrder()->first();
            $par = Participant::where( ['lottery_id' => $lottery->id , 'id' => $winnerTick->participant_id ] )->first();
        }

        $winnerTick->participant;
        
        $totalEnteries = $lottery->tickets()->count();
        
        $participants = $lottery->participants()->get(); 
        
        $data['winner']['participantId'] = $winnerTick->participant_id;
        $data['winner']['ticketNumber'] =  $winnerTick->id;
        $data['winner']['name'] =  $par->name;
        $data['winner']['email'] =  $par->email;


        foreach ($participants as $key => $participant) {
            # code...
            $participants[$key]->tickets = $participant->tickets()->count(); 
            $participants[$key]->probablity =  (int) ( $participants[$key]->tickets/$totalEnteries * 100  );
            if( $winnerTick['participant_id'] == $participant->id )
            {
                $data['winner']['totalEnteries'] = $participants[$key]->tickets;
            }
        }   

        $data['participants'] = $participants;
        $data['totalEnteries'] = $totalEnteries;

        if($flagWithMessage)
        {
            return $this->responseDataM( $data , 'reDrawResultSame' );
        }
        return $this->responseData( $data );

    }

    public function saveDrawResults(Request $request)
    {
        
        $data = $request->all();
        
        if( !$data )
        {
             return $this->errorMessageM('noParticipantExist');
        }

        $lottery =  Participant::find( $data['participantId'] )->lottery()->first();

        $winnerTick = $lottery->tickets()->where('is_winner',1)->first();

        if($winnerTick) return $this->errorMessageM('resultsSavedBefore');

        $ticket = Ticket::where( ['participant_id' => $data['participantId'] , 'id' => $data['ticketNumber']  ] )->first();
        $participant = $ticket->participant()->first();
        
        // $lottery = $ticket->lottery()->first();

        $ticket->is_winner = 1;
        $ticket->save();

        $lottery->is_active = 'compeleted';
        $lottery->save();

        return $this->successMessageM('resultsSaved');

    }
}
