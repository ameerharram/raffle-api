<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Lottery;

class TicketsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lottery = Lottery::where('is_active', 'active')->first();
        $tickets = Ticket::select(
                                   \DB::raw('count(*) as total'),  
                                  'tickets.is_winner',
                                  'tickets.order_number',
                                  'tickets.order_type',
                                  'tickets.participant_id',
                                  'tickets.lottery_id',
                                  'tickets.order_number'
                                 )
                            ->groupBy(['tickets.is_winner',
                                  'tickets.order_number','tickets.order_type','tickets.participant_id','tickets.lottery_id', 'tickets.order_number'])
                            ->where('lottery_id',$lottery->id )
                            ->with(['participant' , 'lottery' => function ($query) {
                                                                    $query->where('is_active', 'active');
                                                                  } 
                                                    ])
                            ->get();

        return $this->responseData( $tickets );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();

        $lottery = Lottery::where('is_active','active')->first();
        if( !$lottery ){ return $this->errorMessageM('activeLotteryFailed'); }
        
        $data['lottery_id'] =  $lottery->id;
        $rules = Ticket::$CREATE_MANUAL_TICKETS_RULES;
        
        $validtor = $this->validateInputs( $data, $rules );
        if($validtor) return $validtor;
        $data['source'] = 'manual';
        $data['order_number'] = 0;
        $data['is_winner'] = 0;
        $data['order_type'] = 'N/A';

        $tickets = $data['number_of_tickets'];
        unset($data['number_of_tickets']);
        for ($i=0; $i < $tickets; $i++) { 
            # code...
            Ticket::create($data);
        }
        return $this->successMessageM('AddedSuccess');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
