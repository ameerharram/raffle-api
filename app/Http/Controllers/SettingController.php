<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Lottery;
use App\Ticket;
use App\Participant;
use DB;

class SettingController extends ApiController
{
    //Moose Membership 100 extra enteries
    //Moose Monthly Premium 350 extra enteries
    public function importOrders(Request $request)
    {
    	$data = $request->all();
    	$rules = ['start' => 'required|date','end' => 'required|date'];
        
        $validtor = $this->validateInputs( $data, $rules );
        if($validtor) return $validtor;
        
        $lottery = Lottery::where('is_active','active')->first();
        if( !$lottery ){ return $this->errorMessageM('activeLotteryFailed'); }

        $params =  array('per_page' => 100,
    					 'page' => 0, 
    					 'order' => 'asc', 
    					 'after' => $data['start'], 
    					 'before' => $data['end'], 
    					);

    	$path = 'orders';
		$counter = 0;
		$flag = true;
		ini_set('max_execution_time', 460000);
		$l = [];
        while ( $flag  ) {
        	# code...
        	$params['page'] = $params['page'] + 1;
			$result = SettingController::httpRequest('GET',$params,$path);
    		
    		$length  = count( $result );
    		$l[$counter] = $length;  
    		if( $length == 0 )
    		{ 

    			$flag = false; 
    		}
    		else
    		{
				SettingController::populateData( $result , $lottery);
				$counter = $counter + 1; 
    		}

		}

    	if( $counter == 0 ){ return $this->errorMessageM('recordImportNotExist'); }
    	
        return $this->successMessageM('recordImportSuccess');	
	}


    public static function processOrder($order_id , $lottery )
    {
        $path = 'orders/' . $order_id;
        $flag = false;
        try{
            $result = SettingController::httpRequest('GET',null,$path);
            $length = count( $result );
            if(  $length != 0  ){
               
                SettingController::populateData( [$result] , $lottery);
                $flag = true; 
            }
        }
        catch (GuzzleException $e) {
        }
        return $flag;
        

    }

    public static function populateData( $result , $lottery)
    {
    	foreach ($result as $key => $v) {
    	    
    		$cusId = $v['customer_id'] > 0 ? $v['customer_id'] : null;
    		$participant = null;
    		
    		$ticket = Ticket::where( ['lottery_id' => $lottery->id, 'order_number' => $v['number'] ] )->first();
    		if( $ticket ){ continue;  }
            /* old logic
    		if( $cusId > 0 && $cusId != null && trim( $cusId ) != '' )
    		{
                $b = $v['billing'];
                $participant = Participant::updateOrCreate(
                    [ 'wp_user_id' => $cusId, 'lottery_id' => $lottery->id ],
                    [ 'name' => $b['first_name'].' '.$b['last_name']  ,'email' => $b['email'] ]
                );  
            }*/
            $b = $v['billing'];
            $participant = Participant::where([ 'lottery_id' => $lottery->id , 
                                                 'email' => $b['email'] 
                                              ])
                                ->first();
            if( $participant )
            {
                $participant->wp_user_id = $cusId;
                $participant->save();

            }
            else
            {
               $participant = Participant::create([
                                                    'lottery_id' => $lottery->id ,
                                                    'name' => $b['first_name'].' '.$b['last_name'],  
                                                    'email' => $b['email'],
                                                    'wp_user_id' => $cusId
                                                 ]); 
            }                    


    		if( !$participant ){ continue; }
			$items = $v['line_items'];

			if( count( $items ) == 0 ){ continue; }
			
			$subscriptions = [ 'Moose Membership', 'Moose Monthly Premium' ];
			$membershipCount = 0;
			
			foreach ($items as $ikey => $i) 
			{
				# code...

				//if( !in_array( $i['name']  , $subscriptions ) || $membershipCount == 0  )
				//{
					$data['lottery_id'] = $lottery->id;
					$data['participant_id'] = $participant->id;
                    $data['order_number'] = $v['number'];
					$data['order_status'] = $v['status'];
                    $data['order_type'] = $i['name'];
					
					if( in_array( $i['name']  , $subscriptions ) )
					{
						//$membershipCount = 1;
						$number = ( $i['name'] == 'Moose Membership' ) ? 120 : 395;
					}
					else
					{
						$number = $i['price'];
					}

					SettingController::createTickets( $data , $number );
				//}

			}

		}
	}

    public static  function createTickets($data , $number )
    {
    	/*Generating Tickets*/
		for($i = 0 ; $i < $number ; $i++ )
    	{
    		Ticket::create( [ 
	    						 'lottery_id' => $data['lottery_id' ],
	    						 'participant_id' => $data['participant_id' ],
	    						 'source' => 'shopping' ,
	    						 'is_winner' => 0 , 
	    						 'order_number' => $data['order_number'],
                                 'order_type' => $data['order_type'],
	    						 'order_status' => $data['order_status']
    					    ]);
    	}
    }


    public static function httpRequest($type,$params,$path,$exheaders = [])
    {
    	$url =  env('WOCOM_API').'/'.$path;
    	
    	$headers = [
					'Content-Type' => 'application/json',
    			   ];
    	$params['consumer_key'] = env('WOCOM_CONSUMER_KEY');		   
    	$params['consumer_secret'] = env('WOCOM_SECRET_KEY');		   
		
		$headers = array_merge($headers,$exheaders);	               
    	$client = new Client(['headers' => ['Accept' => 'application/json']]);
		
		$res = $client->request($type, $url,[

				'headers' => $headers,
		        'query' => $params
		    ]);
		return  json_decode($res->getBody(),true);
	}
}
