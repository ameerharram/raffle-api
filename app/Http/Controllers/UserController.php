<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Auth;
use Config;
use DB;
use Hash;

class UserController extends ApiController
{
    //
    public function userProfile()
    {
    	$user = Auth()->user();
       
    	return $this->responseData( $user );
    }

    public function userList()
    {
        return $this->responseData(User::all());
    	return Datatables::of(collect(User::all()))->make(true);
    	
    }

    public function updateUserInfo(Request $request)
    {
        $data = $request->all();
        $rules = User::$userInfoUpdateRules;

        /* break point if invalidates */
        $validtor = $this->validateInputs($data ,$rules);
        if($validtor) return $validtor;

        $user = Auth()->user();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->gender = $data['gender'];

        if($user->save())
        {
           return  $this->successMessageM('infoUpdated');
        }   

        return $this->errorMessageM('oppFail');

    }

    public function getCompanyInfo()
    {
        $user = Auth()->user();
        
        if($user->user_type != 'company owner')
            return $this->errorMessageM('oppFail');
        
        $company = $user->company()->get();

        return $this->responseData($company);
    }

    public function updateCompanyInfo(Request $request)
    {
        $data = $request->all();
        $rules = Company::$companyUpdate;

        /* break point if invalidates */
        $validtor = $this->validateInputs($data ,$rules);
        if($validtor) return $validtor;
        $user = Auth()->user();
            
        if($user->user_type != 'company owner')
            return $this->errorMessageM('oppFail');
        
        $company = $user->company()->first();
        $company->company_name = $data['company_name'];
        
        if($company->save())
            return $this->successMessageM('infoUpdated');
        return $this->errorMessageM('oppFail');
    }

    public function createEmployee(Request $request)
    {
        $data = $request->all();
        $rules = User::$signupRules;

        /* break point if invalidates */
        $validtor = $this->validateInputs($data ,$rules);
        if($validtor) return $validtor;

        $user = Auth()->user();

        if($user->user_type != 'company owner')
            return $this->errorMessageM('oppFail');
        $company_id = $user->company()->first()->id;
        $data['password'] = Hash::make($data['password']);
        
        $transcation = DB::transaction(function () use ($data,$company_id) {
                            $employee = User::create($data);
                            $employee->Employee()->create(['company_id'=>$company_id ,'designation'=>'employee']);
                        });

        return $this->successMessageM('added');
    }


    public function getEmployees()
    {
        $user = Auth()->user();

        if($user->user_type != 'company owner')
            return $this->errorMessageM('oppFail');
        $company = $user->company()->first();

        if(!$company)
            return $this->errorMessageM('oppFail');
        
        $employees = $company->employees()->with('user')->get();
        return $this->responseData($employees);

        
    }

    public function changeEmployeePassowrd(Request $request)
    {
        $data = $request->all();
        $rules = User::$signupRules;
        return '';
    }

}
