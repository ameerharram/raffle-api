<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ticket;
use App\Lottery;
use App\Participant;


class DashboardController extends ApiController
{
    //
    public function totalAdminUsers()
    {
    	return User::where( 'user_type' , '<>' , 'api'  )->count();
    }

    public function totalLotteries()
    {
    	return Lottery::count();
    }

    public function totalWinners()
    {
    	return Ticket::where( 'is_winner' , 1 )->count();
    }

    public function totalTickets()
    {
    	return Lottery::count();
    }

    public function totalParticipants()
    {
    	return Participant::count();
    }


    public function dashboardStats()
    {
    	$data['users'] = $this->totalAdminUsers();
    	$data['lotteries'] = $this->totalLotteries();
    	$data['winners'] = $this->totalWinners();
    	$data['tickets'] = $this->totalTickets();
    	$data['participants'] = $this->totalParticipants();

    	return $this->responseData( $data );
    	
    }


}
