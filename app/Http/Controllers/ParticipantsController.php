<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participant;
use App\Lottery;



class ParticipantsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lottery = Lottery::where('is_active', 'active')->first();
        $participants = Participant::where('lottery_id',$lottery->id)->with('lottery')->get();
        return $this->responseData( $participants );
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();

        $lottery = Lottery::where('is_active','active')->first();
        if( !$lottery ){ return $this->errorMessageM('activeLotteryFailed'); }
        
        $data['lottery_id'] =  $lottery->id;
        $rules = Participant::PARTICIPANT_CREATE_RULES( $data['lottery_id'] );
        
        $validtor = $this->validateInputs( $data, $rules );
        if($validtor) return $validtor;

        $data['is_manual_entery'] = 1;

        $participant = Participant::create( $data );
        if( !$participant ){ return $this->errorMessageM('somethingWrong'); }
        
        return $this->successMessage('AddedSuccess');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
