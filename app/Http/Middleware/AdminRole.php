<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminRole
{
    static $privilege = 'You do not have sufficient rights to perform this action.';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
       if (Auth::check() && Auth::user()->user_type  == 'admin')
       {
           return $next($request);
       }
       
       return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(AdminRole::$privilege) ], 403);
     
    }
}
