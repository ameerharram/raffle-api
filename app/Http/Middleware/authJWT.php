<?php 
namespace App\Http\Middleware;

use Closure;

use Exception;
use \Tymon\JWTAuth\Exceptions\TokenInvalidException;
use \Tymon\JWTAuth\Exceptions\TokenExpiredException;

use JWTAuth;
use Response;

class AuthJWT
{
    static $tokenNotProvided = 'authentication token is missing.';
    static $tokenInvalid = 'authentication is invalid.';
    static $tokenExpired = 'authentication is expired.';
    static $someProblem = 'some problem occur while authentication of token.';
    static $UserNotFound = 'User not found.';
    

    public function handle($request, Closure $next)
    {

        // Because server was not supporting token module...
        // if ( !$token = $request->get('Bearer') )
        if ( !$token = JWTAuth::setRequest($request)->getToken() )
        {
            return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(
                AuthJWT::$tokenNotProvided)], 400);
        }
        
        try
        {
            $user = JWTAuth::authenticate($token);
            
        }
        catch (Exception $e)
        {
            if ( $e instanceof TokenInvalidException )
            {
                return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(AuthJWT::$tokenInvalid)], 401);
            }
            else if ( $e instanceof TokenExpiredException )
            {
                return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(AuthJWT::$tokenExpired)], 400);
            }
            else
            {
                //fault server error so, forbid the user here...
                return Response::json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(AuthJWT::$someProblem)], 500);
            }
        }

        if ( !$user )
        {
            return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(AuthJWT::$UserNotFound)], 404);
        }
        return $next($request);
    }
}

?>