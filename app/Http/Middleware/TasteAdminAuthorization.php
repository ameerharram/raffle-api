<?php

namespace App\Http\Middleware;
use Closure;
use Auth;
use Response;
class TasteAdminAuthorization
{
    static $message = 'access denied.';
    

    public function handle($request, Closure $next)
    {
        
        if(Auth()->user()->user_type != 'merchant') 
             return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans(
                TasteAdminAuthorization::$message)], 403);
        
        return $next($request);
    }
}
