<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table = 'company_employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id','company_id','designation','created_at','updated_at'
    ];


    public function user(){
        return $this->belongsTo('App\User', 'employee_id','id');
    }


    

}
