<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public static $ADMIN_KEY = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','name', 'email', 'password','profile_picture','gender','user_type','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'confirmation_code'
    ];

    /*Rules for User*/
    public static $CREATE_RULES = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6',
        'user_type' => 'required|in:admin'
    ];



    public static $loginRules = [
        'email' => 'bail|required|email|exists:users,email', 
        'password' => 'bail|required'
    ];


    
    public static $signupRules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6',
        'gender' => 'required|in:male,female',
        'user_type' => 'required|in:employee,project manager'
    ];


    public static $updateBasicInfo = [
        'first_name' => 'required|min:3|max:30',
        'last_name' => 'required|min:3|max:30'
    ];


    public static $updatePicture = [
        'picture' => 'required|max:2000|mimes:jpg,jpeg,png,gif,bmp,tiff'
    ];

    public static $updatePassword = [
        'old_password' => 'required',
        'new_password' => 'required',
        'confirm_password' => 'required|same:new_password'
    ];


    /*RELATIONSHIPS*/
    public function company(){
        return $this->hasOne('App\Company', 'user_id','id');
    }

    public function visitor(){
        return $this->hasOne('App\Visitor', 'visitor_id','id');
    }

    public function employee(){
        return $this->hasOne('App\Employee', 'employee_id','id');
    }

    public function client(){
        return $this->hasOne('App\Client', 'client_id','id');
    }

    


}
