<?php

function getMilliseconds()
{
    $microtime = microtime();
    $comps = explode(' ', $microtime);
    return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
}

function setfileName($extension)
{
    return time() . "_" . getMilliseconds() . "." . $extension;
}

function getExtension($mime_type)
{
    return ['image/jpeg' => 'jpeg',
            'image/jpg' => 'jpg',
            'image/png' => 'png',
            'image/gif' => 'gif',
            'image/bmp' => 'bmp',
            'image/tiff' => 'tiff'
           ][$mime_type];
}

function getExtensionFromUrl($url){
    $extension = pathinfo($url, PATHINFO_EXTENSION);
    $extension = explode('?', $extension)[0];
    return $extension;
}


function getFileMimeTypes()
{
    return ['application/octet-stream', // txt etc
            'application/msword', // doc
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //docx
            'application/vnd.ms-excel', // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // xlsx
            'application/pdf', // pdf
            ];
}

function getPdfMimeType()
{
    return ['application/pdf'];
}

function getExcelMimeType()
{
    return [
            'text/plain',
            'application/vnd.ms-excel', // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
            ];
}

function getMatchedStrings($pathToFile, $searchString)
{
    $matches = [];

    $handle = @fopen($pathToFile, "r");

    if ( $handle )
    {
        while ( !feof($handle) )
        {
            $buffer = fgets($handle);
            
            if ( stripos($buffer, $searchString) !== FALSE )
            {
                $matches[] = str_replace(["\r", "\n"], '', $buffer);
            }
        }
        fclose($handle);
    }

    return $matches;
}

function getFileUploadError($index)
{
    return ['There is no error, the file uploaded with success',
            'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            'The uploaded file was only partially uploaded',
            'No file was uploaded',
            'Missing a temporary folder',
            'Failed to write file to disk.',
            'A PHP extension stopped the file upload.'][$index];
}

function getRouteList($group = 'api')
{
    $routes = [];
    foreach (Route::getRoutes() as $route)
    {
        $route = $route->getPath();
        if ( strpos($route, $group) !== false )
            array_push($routes, explode('/', $route)[1]);
    }

    if ( $group == 'api' )
        unset($routes[0]);  //remove route list 

    return $routes;
}






function objToArray($obj)
{
    // Not an object or array
    if (!is_object($obj) && !is_array($obj)) {
        return $obj;
    }

    // Parse array
    foreach ($obj as $key => $value) {
        $arr[$key] = $this->objToArray($value);
    }

    // Return parsed array
    return $arr;
}

function array_has_dupes($array) {
   return count($array) !== count(array_unique($array));
}