<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    //

    protected $guarded = [];

    /*Rules for User*/
    public static $CREATE_RULES = [
        'name' => 'required|string|max:100',
        'enteries' => 'required|numeric|min:-1',
        'start' => 'required|date',
        //'start' => 'required|date|date_format:"yyyy-mm-dd"',
        'end' => 'required|date',
        'announcment' => 'required|date',
        'description' => 'nullable|string|max:300',
    ];

    /*Relationships*/
    public function tickets()
    {
        return $this->hasMany('App\Ticket','lottery_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function participants()
    {
        return $this->hasMany('App\Participant','lottery_id');

    }
}
