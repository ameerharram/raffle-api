<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{


    //
    protected $table = 'companies';

    protected $guarded = [];

    /*Company validation*/
     const  COMPANY_CREATE = [
                                'company_name' => 'required',
                                'company_description' => 'required'
                             ];

    public function employees(){
        return $this->hasMany('App\Employee', 'company_id','id');
    }

    public static function getClientCompanies($created_by)
    {
        return self::where('created_by',$created_by);
    }

}
