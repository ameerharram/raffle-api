<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
class Participant extends Model
{
    //
    protected $guarded = [];

    /*Rules for User*/
    
    public static $PARTICIPANT_RULES = [
        'wp_user_id' => 'required|numeric',
        'lottery_id' => 'required|numeric|exists:lotteries,id'
    ];

    public static $PARTICIPANT_CREATE_RULES = [
        'name' => 'required|string',
        'wp_user_id' => 'required|numeric|min:1|unique:participants,id',
        'email' => 'required|email|unique:participants,email',
        'lottery_id' => 'required|numeric|exists:lotteries,id'
    ];


    public static function PARTICIPANT_CREATE_RULES( $lottery_id )
    {
        return [

                'name' => 'required|string',
                'wp_user_id' => ['required','numeric','min:1' , 
                                 Rule::unique('participants')->where(function ($query) use ($lottery_id)  {
                                    $query->where('lottery_id', $lottery_id);
                                 })],   
                'email' => ['required','email',
                                Rule::unique('participants')->where(function ($query) use ($lottery_id)  {
                                    $query->where('lottery_id', $lottery_id);
                                 })], 
                'lottery_id' => 'required|numeric|exists:lotteries,id'
               ];

    }


    public function tickets()
    {
        return $this->hasMany('App\Ticket','participant_id');
    }

    public function lottery()
    {
        return $this->belongsTo('App\Lottery','lottery_id');
    }

}
