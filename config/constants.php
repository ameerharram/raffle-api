<?php

/*
|--------------------------------------------------------------------------
| READ URLS
|--------------------------------------------------------------------------
|
*/
//http run on by default 80 port and https on 443 port...
//in laravel case $_SERVER['SCRIPT_NAME'] always contain index.php as the current file for all requests, so, easily able to get base path...
define('READ_BASE_URL', ((!empty(@$_SERVER['HTTPS']) && @$_SERVER['HTTPS'] != 'off') || @$_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://" . @$_SERVER['HTTP_HOST'] . dirname(dirname(@$_SERVER['SCRIPT_NAME']))  . '/public/uploaded_files');

define('READ_PROFILE_PICS', READ_BASE_URL . '/profile_pics/');

define('READ_QOUTE_PERSONS', READ_BASE_URL . '/qoute_persons/');

// define('READ_MENU_PICS', READ_BASE_URL . '/business_pics/menu');

// define('READ_GALLARY_PICS', READ_BASE_URL . '/business_pics/gallary');

// define('READ_DEAL_PICS', READ_BASE_URL . '/business_pics/deals');

// define('READ_EVENT_PICS', READ_BASE_URL . '/business_pics/event');

/*
|--------------------------------------------------------------------------
| WRITE PATHS
|--------------------------------------------------------------------------
|
*/
define('WRITE_UPLOADED_FILES_BASEPATH', realpath(base_path() . '/public/uploaded_files/'));

define('WRITE_PROFILE_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/profile_pics/');

define('WRITE_QOUTE_PERSONS',WRITE_UPLOADED_FILES_BASEPATH . '/qoute_persons/');

// define('WRITE_MENU_PICS',WRITE_UPLOADED_FILES_BASEPATH . '/business_pics/menu');

// define('WRITE_GALLARY_PICS',WRITE_UPLOADED_FILES_BASEPATH . '/business_pics/gallary');

// define('WRITE_DEAL_PICS',WRITE_UPLOADED_FILES_BASEPATH . '/business_pics/deals');

// define('WRITE_EVENT_PICS',WRITE_UPLOADED_FILES_BASEPATH . '/business_pics/event');

/*
|--------------------------------------------------------------------------
| TEST FILES PATH
|--------------------------------------------------------------------------
|
*/
// define('TEST_FILES_BASEURL', realpath(base_path() . '/test_files/'));

return [
	  
        'WRITE_PROFILE_PICS' => WRITE_PROFILE_PICS,
	   'READ_PROFILE_PICS' => READ_PROFILE_PICS,
     
        'WRITE_QOUTE_PERSONS' => WRITE_QOUTE_PERSONS,
        'READ_QOUTE_PERSONS' => READ_QOUTE_PERSONS
     ];