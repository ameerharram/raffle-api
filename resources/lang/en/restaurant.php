<?php

return [
    'insertionSuccessfull' => 'Restaurant Inserted Successfully.',
    'updationSuccessfull' => 'Restaurant Updated Successfully.',
    'deletionSuccessfull' => 'Restaurant Deleted Successfully.',
    'deletionError' => 'Restaurant Not Deleted Successfully.',

];