<?php
return [
	

    /*Auth Messaged*/
    'loggedIn' => 'Logged In.',
    'loggedOut' => 'Logged Out.',
    'infoUpdated'=> 'Info updated.',

    'oppFail'=> 'Opperation failed.',
    'invalidPassword'=> 'Invalid password.',

    'passwordUpdated'=> 'Password updated.',
    
    'added'=> 'Record Added.',

    'companyNotExist' => 'Selected company not exist in system.',

    'resultsSaved' => 'Result saved successfully.',
    'resultsSavedBefore' => 'Result saved already.',
    'reDrawResultSame' => 'Showing already saved result.',
    
    'winnerNoTExist' => 'Winner not drawn yet.',
    'noParticipantExist' => 'No Participants found.',
    'activeLotteryExistt' => 'Active lottery already exist.',

    /*General Messages*/
    'errorMessage' => 'The following validations are failed.',
    'somethingWrong' => 'Some thing went wrong, please try latter.',
    'wrongCredentials' => 'Either email is wrong or password.',
    'userDelete' => 'User has been successfuly deleted.',

    'imageFail' => 'Some thing went wrong, Unable to upload image.',

    'AddedSuccess' => 'Record successfully added.',
    'AddedFail' => 'Some thing went wrong, unable to add record.',

    'recordUpdateSuccess' => 'Record updated successfully.',
    'recordUpdateFailed' => 'Some thing went wrong, unable to update record.',

    'deleteSuccess' => 'Record deleted successfully.',
    'deleteFaile' => 'Some thing went wrong, unable to delete record.',

    'recordImportSuccess' => 'Record imported successfully.',
    'recordImportNotExist' => 'Record not found for import.',
    'activeLotteryFailed' => 'No Active Lottery exist!',
    
    /*Account verification */
    'userVerified' => 'Your account has been already verified, You maylogin your account.',
    'verifyAccount' => 'Please verify your email for login.', 
    'verifcationSuccess' => 'Your email has been verified successfully, please wait you will be directed to login screen.',
    /*Tast Merhcant Admin*/ 
    'userProfileUpdated'  => 'Profile info updated successfully.',
    'userPictureUpdated'  => 'Picture updated successfully.',
    'userPasswordUpdated' => 'Password has been updated successfully.',
    'userPasswordUpdateProblem' => 'Current password is invalid.',

    /* Email Messages*/
    'signupMessage' => 'Email has been sent to your email address, please verify your email.',

    /* Business messages */

    'BusinessinsertionSuccessfull' => 'Business Inserted Successfully.',
    'BusinessupdationSuccessfull' => 'Business Updated Successfully.',
    'BusinessdeletionSuccessfull' => 'Business Deleted Successfully.',
    'BusinessdeletionError' => 'Business Not Deleted Successfully.',
    'BusinessinsertionGallery' => 'Business inserted Successfully',
    'BusinessinsertionBusinessSuccessfull' => 'Business Inserted Successfully.',
    'BusinessupdationBusinessSuccessfull' => 'Business Updated Successfully.',
    'BusinessrestaurantPictureUpdateProblem' => 'Business Picture Problem.',
    'businessExist' => 'Business under same type already exist.',


    /*restaurant messages */
    'insertionSuccessfull' => 'Restaurant Inserted Successfully.',
    'updationSuccessfull' => 'Restaurant Updated Successfully.',
    'deletionSuccessfull' => 'Restaurant Deleted Successfully.',
    'deletionError' => 'Restaurant Not Deleted Successfully.',
    'insertionGallery' => 'Gallery inserted Successfully',
    'insertionBusinessSuccessfull' => 'Business Inserted Successfully.',
    'updationBusinessSuccessfull' => 'Business Updated Successfully.',
    'restaurantPictureUpdateProblem' => 'Picture Problem.',

    /*event messages*/
    'eventInsertSuccessfull' => 'Events Inserted Successfully.',
    'updationEventSuccessfull' => 'Events Updated Successfully.',
    'deleteEventSuccessfull' => 'Event Deleted Successfully.',
    
    'businessMissed' => 'Please select business type.',

    /* Gallery message*/
    'GalleryinsertionSuccessfull' => 'Gallery Inserted Successfully.',
    'GalleryupdationSuccessfull' => 'Gallery Updated Successfully.',
    'GallerydeletionSuccessfull' => 'Gallery Deleted Successfully.',
    'GallerydeletionError' => 'Gallery Not Deleted Successfully.',

    /* Deal */
    'DealUpdation' => 'Menu updated successfully.',

    /*Advertise with us*/
    'advertise' => 'Advertise data have been successfully submitted.',
    
];