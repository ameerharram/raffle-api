
             @extends('emails.common.app')                                           
             

@section('content')
    
                                                        <!-- content -->
              <h2 style="word-wrap: normal; font-family: arial; font-weight: 700; margin-bottom: 0px; font-size: 20px; text-align: left; color: #051c2c; line-height: 1.3;padding-left: 25px;    margin-bottom: 20px;margin-top: -20px;text-transform: uppercase;">Thank for your <strong style="letter-spacing: 3px;font-weight: 900;">joining us!</strong></h2>
              <h2 style="word-wrap: normal; font-family: arial; font-weight: 700; margin-bottom: 10px; font-size: 18px; text-align: left; color: #666666; line-height: 1.3;    margin: 0;padding-left: 25px; ">Hi {{ $user->first_name }} {{ $user->last_name }}!</h2>

              <p style="font-size: 14px; line-height: 30px; color: #666666; margin-bottom: 10px; font-weight: normal; padding: 0; text-align: left;padding-left: 25px;    margin: 20px 0; ">
                Welcome to BYOB! As a team, we thank you for your interest in our website and would like to welcome you to our services.
             </p>
            <p style="font-size: 14px; line-height: 30px; color: #666666; margin-bottom: 10px; font-weight: normal; padding: 0; text-align: left;padding-left: 25px;    margin: 20px 0; ">
             Here at BYOB we want to help revolutionize the way you look at and go about business. Using our services, we hope that you will be able to build something for yourself, or improve what is already there. We are constantly working to improve the user experience and to help you, and others like you, grow and prosper. Now, get networking and be your own boss! </p>
              <p style="font-size: 14px; line-height: 30px;color: #666666; margin-bottom: 10px;padding-left: 25px;  font-weight: normal; padding: 0; text-align: left;padding-left: 25px;     margin: 20px 0;">Your account is created and must be activated before you can use it. To activate your account click on the following link or copy and paste it into your browser...</p>
                  
              <p style="font-size: 14px; line-height: 1.3; color: #666666; margin-bottom: 10px; font-weight: normal; padding: 0; text-align: left;padding-left: 25px;     margin: 30px 0 40px 0;"><a style="color: #0f8ae0;font-style: italic;" href="{{$user->confirmation_code}}">{{$user->confirmation_code}}</a>.</p>
                                                        
@endsection
