<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <style>

    </style>
</head>

<body style="background:#0e0e0e!important;font-size:14px;line-height:1.3;color:#666666;font-family:arial;width:100%!important;min-width:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;font-weight:normal;text-align:left;" class="body">
    <!-- <style> -->
    <center data-parsed="">
        <table class="container float-center" style="margin: 0 auto; float: none; background: #fefefe; width: 640px; padding: 0; vertical-align: top; text-align: center; display: table; border-spacing: 0; border-collapse: collapse;" align="center">
            <tbody>
                <tr style="padding: 0; vertical-align: top; text-align: left;">
                    <td>
                        <table class="spacer" style="width: 100%; padding: 0; display: table; vertical-align: top; border-spacing: 0; border-collapse: collapse; text-align: left;">
                            <tbody>
                                <tr style="padding: 0; vertical-align: top; text-align: left;">
                                    <td style="font-size: 16px; line-height: 16px;" height="16px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="row" style="display: table; padding: 0; vertical-align: top; text-align: left; width: 100%; position: relative; border-spacing: 0; border-collapse: collapse;">
                            <tbody>
                                <tr style="padding: 0; vertical-align: top; text-align: left;">
                                    <th class="small-12 large-12 columns first last" style="padding-right: 30px; padding-left: 30px; padding-bottom: 0px; width: 564px;">
                                        <table style="width: 100%; padding: 0; vertical-align: top; text-align: left;">
                                            <tbody>
                                                <tr style="padding: 0; vertical-align: top; text-align: left;">
                                                    <th>
                                                        <table class="callout" style="width: 100%; padding: 0; vertical-align: top; text-align: left; margin-bottom: 0px; display: table; border-spacing: 0; border-collapse: collapse;">
                                                            <tbody>
                                                                <tr style="padding: 0; vertical-align: top; text-align: left;">
                                                                    <td class="callout-inner primary" style="background: #051c2c; color: #fff; padding:  15px; width: 50%;"><span style="color: #fff; text-align: left;"><img src="{{ asset('/images/logo.png') }}" alt="" /> </span></td>
                                                                    <td class="callout-inner primary" style="background: #051c2c;color: #fff;padding: 26px 15px 10px 10px;width: 50%;text-align: right;"><span style="color: #fff; text-align: right;">{{$user->current_date}}</span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <img src="{{ asset('/images/banner.jpg') }}" width="auto" height="auto" />
                                                        

                                                        @yield('content')


                                                        <h5 style="padding-left: 25px; font-size: 16px; color: inherit; word-wrap: normal; font-family: arial; font-weight: normal; margin-top: 10px; margin-bottom: 18px;"><img src="{{ asset('/images/signature.png') }}" width="auto" height="auto" /></h5>
                                                        <!-- content -->
                                                        <p style="font-size: 14px; line-height: 30px; color: #666666; margin-bottom: 10px; font-weight: normal; padding: 0; text-align: left; font-style: italic;padding-left: 25px; margin: 20px 0 0  0;">Matthew William Torres</p>
                                                        <p style="font-size: 14px; line-height: 30px; color: #666666; margin-bottom: 10px; font-weight: normal; padding: 0; text-align: left;padding-left: 25px; margin:0 0 20px 0; font-style: italic;">Founder and CEO</p>
                                                        <table class="callout" style="width: 100%; margin-bottom: 16px; padding: 0; vertical-align: top; text-align: left; border-spacing: 0; border-collapse: collapse;">
                                                            <tbody>
                                                                <tr style="padding: 0; vertical-align: top; text-align: left;">
                                                                    <th class="callout-inner secondary " style="background: #051c2c; border: 1px solid #483c32; color: #fff; padding: 20px; width: 100%; font-size: 14px; line-height: 1.3; font-weight: normal; vertical-align: top; text-align: left; word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; font-family: 'Open Sans', sans-serif;">
                                                                        <!-- <p style="color: #fff; font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 14px; margin-bottom: 10px; line-height: 1.3; margin-top: 0; vertical-align: top; text-align: left; padding: 0; word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important;">Address: 311 East 23 Rd St New York, NY 1</p> -->
                                                                        <span style="margin-right: 20px; color: #fff;">Tell: <a style="color: #fff; font-family: 'Open Sans', sans-serif; text-decoration: none; font-weight: 600;" href="tel:+19172881525">+1  (917) 288 - 1525 </a></span> 
                                                                        <!-- <span style="margin-right: 20px; color: #fff;"><a style="color: #fff; font-family: 'Open Sans', sans-serif; text-decoration: none; font-weight: 600;" href="tel:+19172881525">+1  (917) 288 - 1525 </a></span>  -->
                                                                        <span style="color: #fff;">Email: <a style="text-decoration: none; color: #fff; font-family: 'Open Sans', sans-serif; font-weight: 600;" href="mailto:Email: info@byob.com">Email: info@byob.com</a></span></th>
                                                                    <th class="expander" style="visibility: hidden; width: 0; padding: 0 !important;">&nbsp;</th>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </th>
                                                    <th class="expander" style="visibility: hidden; width: 0; padding: 0 !important;">&nbsp;</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                        <center data-parsed="">
                            <table class=" footer" style="width: 90%; padding: 0; vertical-align: top; text-align: left; border-spacing: 0; border-collapse: collapse;" align="">
                                <tbody>
                                    <tr style="padding: 0; vertical-align: top; text-align: left;">
                                        <td style="font-size: 14px; line-height: 1.3; color: #666666; font-weight: normal; padding: 0; vertical-align: top; text-align: left; word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important;">
                                            <table style="width: 100%; padding: 0; vertical-align: top; text-align: left;">
                                                <tbody>
                                                    <tr style="padding: 0; vertical-align: top; text-align: left;">
                                                        <td style="font-size: 14px; line-height: 1.3; color: #666666; font-weight: normal; padding: 0; vertical-align: top; text-align: left; word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important;">
                                                            <p style="font-size: 14px; line-height: 1.3; color: #666666; margin: 0 0 20px 0; font-weight: normal; padding: 0; text-align: left;">&copy;Copyright 2017 - Byob - All Rights Reserved </p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <table style="width: 100%; padding: 0; vertical-align: top; text-align: left;">
                                                <tbody>
                                                    <tr style="padding: 0; vertical-align: top; text-align: left;">
                                                        <td style="font-size: 14px; line-height: 1.3; color: #666666; font-weight: normal; padding: 0; vertical-align: top; text-align: left; word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important;">
                                                            <p style="font-size: 14px; line-height: 1.3; color: #666666; margin-bottom: 10px; font-weight: normal; padding: 0; margin: 0; text-align: right;"><a href="https://twitter.com/docscott49" target="_blank" rel="noopener noreferrer"><img src="https://assets.privy.com/picture_photos/334463/medium/9ba00a15d0174a948a9c4e93f5ad0972?1496744827" width="auto" height="auto" /></a>&nbsp;&nbsp; <a href="https://www.facebook.com/BishopLeonardScott/" target="_blank" rel="noopener noreferrer"><img src="https://assets.privy.com/picture_photos/334467/medium/130201e3546d427bac81610577e822be?1496744904" width="auto" height="auto" /></a>&nbsp;&nbsp; <a href="https://plus.google.com/107292307879507045784" target="_blank" rel="noopener noreferrer"><img src="https://assets.privy.com/picture_photos/334468/medium/840ca6549f904755b34cc29ac0271173?1496744918" width="auto" height="auto" /></a>&nbsp;&nbsp;&nbsp;<a href="https://www.youtube.com/bishopleonardscott" target="_blank" rel="noopener noreferrer"><img src="https://assets.privy.com/picture_photos/334470/medium/d057d4b38fcb458a8cc50b11ad993d1e?1496744976" width="auto" height="auto" /></a>&nbsp;&nbsp;&nbsp;<a href="https://www.linkedin.com/company/bishop-leonard-scott-ministries?trk=tabs_biz_home" target="_blank" rel="noopener noreferrer"><img src="https://assets.privy.com/picture_photos/334469/medium/c1b85853679945e2af2921cd11ef094a?1496744928" width="auto" height="auto" /></a>&nbsp; <a href="https://www.instagram.com/docscott49/" target="_blank" rel="noopener noreferrer"><img src="https://assets.privy.com/picture_photos/334471/medium/a843cc14fefb41dcadfd873c81041a3d?1496744988" width="auto" height="auto" /></a></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
    <!-- prevent Gmail on iOS font size manipulation -->
    <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>

</html>